
#%%
get_ipython().run_line_magic('load_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')


#%%
import os


#%%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


#%%
import utils


#%%
data_path = '../test/data/'
data_files = os.listdir(data_path)


#%%
df = utils.load_csv_('../test/data/cpu_cpu_used_pct_8.csv')


#%%
utils.view_data(df.loc[:'2018-09-30'].values, figsize=(16, 4), title=df.columns[0])


