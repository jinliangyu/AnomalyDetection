# -*- coding: utf-8 -*-
import json
import os
from datetime import datetime
from functools import wraps
from time import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def timer(method):
    """
    Decorator for timing.
    """

    @wraps(method)
    def function_timer(*args, **kwargs):
        print('[{name}] started at {now}'.format(
            name=method.__name__, now=datetime.now()))
        t0 = time()
        result = method(*args, **kwargs)
        t1 = time()
        print('[{name}] finished at {now}. Cost time: {time:.2f}s'.format(name=method.__name__,
                                                                          now=datetime.now(), time=t1 - t0))
        return result

    return function_timer


def smooth(x, window_len=11, window='hanning'):
    """Smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the start and end part of the output signal.

    input:
        x: the input signal 
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")
    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")
    if window_len < 3:
        return x
    if window not in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError(
            "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")
    s = np.r_[x[window_len - 1:0:-1], x, x[-2:-window_len - 1:-1]]
    if window == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('np.' + window + '(window_len)')
    y = np.convolve(w / w.sum(), s, mode='valid')
    return y[(window_len - 1) // 2:-(window_len - 1) // 2]


def get_date_list(df):
    """
    return a list of date from DataFrame with format '%Y-%m-%d'.
    :param df: pd.DataFrame
    :return: sorted list of date
    """
    return sorted({i.strftime('%Y-%m-%d') for i in df.index})


def row2col(row_data):
    """
    Change "row data" to "column data".
    Here, "row data" refers to the data which is saved by "Series as rows",
    "col data" refers to "Series as columns" refering to Grafana.
    :param row_data: {pd.DataFrame} -- series as rows, e.g.:
            =======================
            Series  | Time | Value
            -----------------------
            metirc1 | ***  | ***
            -----------------------
            metric2 | ***  | ***
            -----------------------
               .    |  .   |  .
               :    |  :   |  :
            -----------------------
            metricn | ***  | ***
            =======================
    :return: col_data {pd.DataFrame} -- series as columns, e.g.:
            =======================================
            Time | metric1 | metric2| ... | metricn
            ---------------------------------------
            ***  |  ***    |  ***   | ... |   ***
            =======================================
    """

    groups = row_data.groupby('Series')
    frames = []
    for i, j in groups.groups.items():
        frame = pd.DataFrame(data=row_data.loc[j.values, 'Value'].values,
                             index=pd.to_datetime(
                                 row_data.loc[j.values, 'Time']),
                             columns=[i])
        frames.append(frame)
    return pd.concat(frames, axis=1)


def comma2semicolon(filename):
    """
    Replace .csv file' separator form ',' to ';', to be unique.
    """
    with open(filename) as f:
        print('filename: {}'.format(os.path.split(filename)[-1]), end='\t')
        if ';' not in f.readline():
            print('(,==> ;)')
            df = pd.read_csv(filename, sep=',', index_col=0)
            df.to_csv(filename, sep=';')
        else:
            print('(already ;)')


def load_csv(filename, time_delta='0h'):
    """
    Load csv files to pd.DataFrame.
    """
    if not filename.endswith('.csv'):
        return None
    with open(filename) as f:
        first_line = f.readline()
        if first_line == 'sep=;\n':
            second_line = f.readline()
            if second_line == 'Series;Time;Value\n':
                print('Do row2col!')
                data = row2col(pd.read_csv(filename, sep=';', skiprows=1))
            else:
                data = pd.read_csv(filename, index_col=0, sep=';', skiprows=1)
        else:
            comma2semicolon(filename)
            data = pd.read_csv(filename, index_col=0, sep=';')
    data.index = pd.to_datetime(data.index) + pd.Timedelta(time_delta)
    return data


def load_csv_(filename, time_delta='0h'):
    """
    [New] Load csv files to pd.DataFrame.
    """
    if not filename.endswith('.csv'):
        return None

    def date_parser(x): return pd.to_datetime(x) + pd.Timedelta(time_delta)
    with open(filename) as f:
        first_line = f.readline()
        if first_line == 'sep=;\n':
            second_line = f.readline()
            if second_line == 'Series;Time;Value\n':
                print('Do row2col!')
                data = row2col(pd.read_csv(filename, sep=';',
                                           skiprows=1, date_parser=date_parser))
            else:
                data = pd.read_csv(filename, index_col=0,
                                   sep=';', skiprows=1, date_parser=date_parser)
        else:
            comma2semicolon(filename)
            data = pd.read_csv(filename, index_col=0, sep=';')
    return data


def load_txt(filename, time_delta='0h', export_csv=False):
    with open(filename) as f:
        d = json.load(f)[0]
    metric = d['metric']
    tags = d['tags']
    kpi_name = metric + \
        '{{{}}}'.format(', '.join(['='.join([k, v]) for k, v in tags.items()]))
    dps = d['dps']
    timestamps, data = zip(*sorted(dps.items()))
    # timestamps, data = zip(*dps.items())
    df = pd.DataFrame(list(data),
                      index=pd.to_datetime(
                          timestamps, unit='s') + pd.Timedelta(time_delta),
                      columns=[kpi_name],
                      dtype='f8')
    df.index.name = 'Time'
    if export_csv:
        df.to_csv(filename.replace('txt', 'csv'), sep=';')
    return df


def view_data(data, figsize=(14, 7), title=None):
    """
    Have a look at the data of numpy.array/pd.DataFrame.
    :param data: data to show
    :return:
    """
    if isinstance(data, pd.DataFrame):
        data.plot(figsize=figsize)
    else:
        plt.figure(figsize=figsize)
        plt.plot(data)
    if title:
        plt.title(title)
    plt.show()


def to_date_list(datetimeindex):
    """Convert datetimeindex to list of date.

    Arguments:
        datetimeindex {pd.DateTimeIndex} -- pandas DateTimeIndex

    Returns:
        list of str -- date list
    """

    return sorted({i.strftime('%Y-%m-%d') for i in datetimeindex})


def date_list(start, end, freq='D'):
    """Use pandas.date_range to generate date_list.

    Arguments:
        start {str} -- Left bound for generating dates
        end {str} -- Right bound for generating dates

    Returns:
        list of str -- list of date
    """

    return to_date_list(pd.date_range(start=start,
                                      end=end,
                                      freq=freq))


def generate_workdate_list(date_list, date_ignore=()):
    """
    return workday date by filtering date_list
    """
    if isinstance(date_list, list) and isinstance(date_list[0], str):
        bussiness = {i.strftime(
            '%Y-%m-%d') for i in pd.date_range(start=date_list[0], end=date_list[-1], freq='B')}
        return sorted(set(bussiness) - set(date_ignore))
    else:
        raise Exception('Argument must be a non-empty list of str!')


def get_workdate_list(df, date_ignore=()):
    """
    return a list of workdate (workday) from DataFrame with format '%Y-%m-%d'
    """
    return generate_workdate_list(get_date_list(df), date_ignore=date_ignore)


def get_workday_data(df, date_ignore=()):
    """Return dataframe of working date.

    Arguments:
        df {pd.DataFrame} -- original dataframe

    Returns:
        pd.DataFrame -- dataframe which only contains working date data 
    """

    workdate_list = get_workdate_list(df, date_ignore=date_ignore)
    return pd.concat([df.loc[i] for i in workdate_list], axis=0) if workdate_list else None


def check_data(df, verbose=0):
    """Check the quality the dataframe.

    Arguments:
        df {pd.DataFrame} -- dataframe to be checked

    Keyword Arguments:
        verbose {int} -- 1: show details of each day 
                         0: show summary information
                         (default: {0})
    """

    date_list = get_date_list(df)
    missing_date = []
    for i in date_list:
        data = df.loc[i]
        nans = float(pd.isnull(data).sum().sum())
        if verbose == 1:
            print('{!r}: Shape: {!r} . NaN: {:.0f}({:.2f}%)'.format(
                i, data.shape, nans, nans / data.size * 100))
        if data.shape[0] != 1440:
            missing_date.append(i)
    print('Date Range: [{!s}] ~ [{!s}]'.format(date_list[0], date_list[-1]))
    print('Columns: {!s}'.format(df.columns.tolist()))
    print('Points Per Day: {:g}'.format(df.shape[0] / len(date_list)))
    if verbose == 1:
        print(
            'Missing Data: {!r}/{!r}({:.2f}%)\n{!r}\nNan Percentage: {:.2f}%'.format(len(missing_date), len(date_list),
                                                                                     len(missing_date) / len(
                                                                                         date_list) * 100, missing_date,
                                                                                     float(pd.isnull(
                                                                                         df).sum().sum()) / df.size * 100))
        return (
            'Missing Data: {!r}/{!r}({:.2f}%)\n{!r}\nNan Percentage: {:.2f}%'.format(len(missing_date), len(date_list),
                                                                                     len(missing_date) / len(
                                                                                         date_list) * 100, missing_date,
                                                                                     float(pd.isnull(
                                                                                         df).sum().sum()) / df.size * 100))
    else:
        print('Missing Data: {!r}/{!r}({:.2f}%)\nNan Percentage: {:.2f}%'.format(len(missing_date), len(
            date_list), len(missing_date) / len(date_list) * 100, float(pd.isnull(df).sum().sum()) / df.size * 100))
        return('Missing Data: {!r}/{!r}({:.2f}%)\nNan Percentage: {:.2f}%'.format(len(missing_date), len(
            date_list), len(missing_date) / len(date_list) * 100, float(pd.isnull(df).sum().sum()) / df.size * 100))


def fillnan(df):
    """Fill the missing data poinrts with NaN.

    Arguments:
        df {pd.DataFrame} -- dataframe with missing data, assuming that each day has 1440 points

    Returns:
        pd.DataFrame -- dataframe without missing data points
    """

    date_list = get_date_list(df)
    frames = []
    for date in date_list:
        new_idx = pd.date_range(start=date + ' ' + '00:00:00',
                                end=date + ' ' + '23:59:59', freq='T')
        # default fill np.NaN
        frames.append(df.loc[date].reindex(new_idx))
    return pd.concat(frames, axis=0)


def filter_data(df, max_nan=0.1):
    """Filter data by max_nan.

    Arguments:
        df {pd.DataFrame} -- dataframe with NaNs

    Keyword Arguments:
        max_nan {float} -- max NaN percentage (default: {0.1})

    Returns:
        pd.DataFrame -- dataframe with NaN percentage under max_nan
    """

    date_list = get_date_list(df)
    frames = []
    for i in date_list:
        data = df.loc[i]
        nan_percentage = float(pd.isnull(data).sum().sum()) / data.size
        if nan_percentage <= max_nan:
            frames.append(data)
    return pd.concat(frames, axis=0) if frames else None


def preprocess(df, date_ignore=[], max_nan=0.1):
    """Preprocess original dirty data.

    Arguments:
        df {pd.DataFrame} -- original dirty data

    Keyword Arguments:
        date_ignore {list of str} -- no-working date except weekends (default: {[]})
        max_nan {float} -- the max percentage of NaNs (default: {0.1})

    Returns:
        pd.DataFrame -- clear data
    """

    return filter_data(fillnan(get_workday_data(df=df,
                                                date_ignore=date_ignore)),
                       max_nan=max_nan).interpolate(limit_direction='both')


def generate_df(ndarray, date, columns):
    """Generate a day dataframe by a 1440-size np.ndarray, a date and a column name.

    Arguments:
        ndarray {np.ndarray} -- a ndarray which size is 1440
        date {str} -- the date
        column {list of str} -- the metric name

    Returns:
        pd.DataFrame -- the dataframe of a day
    """

    return pd.DataFrame(data=ndarray,
                        index=pd.date_range(
                            start=' '.join([date, '00:00:00']),
                            end=' '.join([date, '23:59:59']),
                            freq='T'),
                        columns=columns)


def df2cols(df):
    """Generator for creating dataframe's each column.

    Arguments:
        df {pd.DataFrame} -- a dataframe with one or more columns

    Yields:
        str, pd.DataFrame -- column name with its dataframe
    """

    for col in df.columns:
        yield str(col), df[[col]]


def fix_df(df, method='default'):
    """
    Repair data by filling NaN and interpolation(linear).

    method(str): default, more
    """
    if method == 'default':
        df_re = df.reindex(pd.date_range(start=df.index[0],
                                         end=df.index[-1],
                                         freq='T'))
    elif method == 'more':
        date_list = sorted({i.strftime('%Y-%m-%d') for i in df.index})
        frames = []
        for date in date_list:
            new_idx = pd.date_range(start=date + ' ' + '00:00:00',
                                    end=date + ' ' + '23:59:59',
                                    freq='T')
            frames.append(df.loc[date].reindex(new_idx))
        df_re = pd.concat(frames, axis=0)
    else:
        raise ValueError(
            "Wrong method! Only two methods supported: default or more!")
    return df_re.interpolate(limit_direction='both')


def adjust_up(x, k):
    """
    Adjust upper.
    """
    return (1 + k * x / x.max()) * x


def adjust_low(x, k):
    """
    Adjust lower.
    """
    return (1 - k * x / x.max()) * x


def adjust(x, method='upper', degree=0.1):
    """
    [New] Adjust lines upper or lower.
    """
    if degree < 0 or degree > 1:
        raise ValueError('Degree must be between 0 and 1!')
    if method == 'upper':
        return (1 + degree * x / x.max()) * x
    elif method == 'lower':
        return (1 - degree * x / x.max()) * x
    else:
        raise ValueError(
            'Method {} is not supported! Choose upper or lower.'.format(method))


if __name__ == "__main__":
    df = load_csv_('test/data/cpu_cpu_used_pct_8.csv')
    df = load_csv('test/data/cpu_cpu_used_pct_8.csv')
    df.info()
    print(df.head())
    # df.plot()
    # plt.show()
