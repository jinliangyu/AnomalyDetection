import pandas as pd

from dta import DTA

# import numpy as np
# import matplotlib.pyplot as plt

# import datatools as dt

"""
Anomaly Detector
"""

__version__ = '1.0'


class AnomalyDetector(object):
    def __init__(self, data_frame=None):
        # self._df_list = self._tolist(data_frame)
        self._metrics = None
        self._calc = False
        self._lines = None    # dict which contains dynamic threshold
        if data_frame is not None:
            self.fit(data_frame)

    def df2cols(self, df):
        for col in df.columns:
            yield df[[col]]

    def fit(self, data_frame=None):
        if data_frame is not None:
            self._dta_list = {str(i.columns[0]): DTA(df=i, name=str(
                i.columns[0])) for i in self.df2cols(data_frame)}
            self._metrics = [str(i.columns[0])
                             for i in self.df2cols(data_frame)]
            print('DTA classes {!r} Initialized!'.format(self._metrics))

    def detect(self, data_frame, verbose=0):
        frames = []
        columns = data_frame.columns.tolist()
        for i in columns:
            if i in self._metrics:
                frames.append(self._dta_list[i].detect(
                    data_frame[[i]], verbose=verbose))
            else:
                print(
                    'There no threshold for metric: {!s}\nSkipped!'.format(i))
        return pd.concat(frames, axis=1)

    @property
    def lines(self):
        if self._calc:
            return self._lines
        for dta in self._dta_list.values():
            dta.calculate()
        self._calc = True
        self._lines = {i: self._dta_list[i].lines for i in self._metrics}
        return {i: self._dta_list[i].lines for i in self._metrics}

    def adujust(self, kwargs):
        if self._calc:
            for metric, k in kwargs.iteritems():
                if k > 0:
                    print('upper')
                    self._dta_list[metric].adjust(method='upper', degree=k)
                elif k < 0:
                    print('lower')
                    self._dta_list[metric].adjust(method='lower', degree=-k)
                else:
                    print('reset')
                    self._dta_list[metric].adjust(method='reset')
            self._lines = {i: self._dta_list[i].lines for i in self._metrics}
        else:
            raise Exception('Lines to be calculated!')
