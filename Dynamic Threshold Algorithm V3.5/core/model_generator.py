# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import pickle
import matplotlib as plt


def kNN(X, mu, n=5):
    """
    Return base_data
    n is length of base_data, default is 5
    """
    dist = ((X - mu) ** 2).sum(axis=1)
    idx_n = np.argsort(dist)[:n]
    return X[idx_n]


def normalize_data(data_ndarray):
    """
    Standardization the np.ndarray data (with nan) 

    Inputs:
    - data_ndarray: np.ndarray data which shape is (days x pdd)

    Outputs:
    - Standardization data the same shape with data_ndarray
    """
    return ((data_ndarray.T - np.nanmean(data_ndarray, axis=1)) / np.nanstd(data_ndarray, axis=1)).T


def filter_data(data, workdate_list, trading_time=('09:00:00', '15:59:59')):
    """
    Description:
        Return filtered data of workday during trading time
    
    Inputs:
        - data (pd.DataFrame): Data Loaded by function 'load_data'
        - workdate_list (list): A list of "Trading Days" of the data
        - trading_time (tuple): Trading time, format: (start_time, end_time)
    
    Outputs:
        - filtered_data (pd.DataFrame): Filtered data of workday during trading time
    """
    filtered_data = pd.concat([data.loc[date + ' ' + trading_time[0]
    :date + ' ' + trading_time[1]]
                               for date in workdate_list])
    return filtered_data


def generate_model(data, workdate_list, trading_time=('09:00:00', '15:59:59'), k=3):
    """
    Calculate base_data and sigma, and generate the model
    """
    # checked_data = check_data(data, workdate_list, show_info=True, return_data=True)
    filtered_data = filter_data(data, workdate_list=workdate_list, trading_time=trading_time)
    check_data(filtered_data, workdate_list, show_info=True, return_data=False)
    days = len(workdate_list)

    # ppd = filtered_data.size / days
    ppd = 1440
    filtered_data_ndarray = filtered_data.values.reshape(-1, ppd)
    # filtered_data_ndarray_std = normalize_data(filtered_data_ndarray)
    filtered_data_ndarray_std = filtered_data_ndarray
    mu = np.nanmean(filtered_data_ndarray_std, axis=0)
    base_data_short = kNN(filtered_data_ndarray_std, mu)
    # base_data_long = kNN(filtered_data_ndarray, np.nanmean(filtered_data_ndarray[-30:, :], axis=0), n=30)
    sigma = np.nanstd(base_data_short, axis=0)
    return dict(base_data=base_data_short, sigma=k * sigma)


def save_model(model, filename):
    """
    Save model
    """
    with open(filename, 'wb') as f:
        pickle.dump(model, f)


# define global variable
chinese_holiday = {i.strftime('%Y-%m-%d') for i in pd.date_range(start='2017-10-1', end='2017-10-8', freq='D')}


# chinese_workday = {i.strftime('%Y-%m-%d') for i in pd.date_range(start='2017-7-8', end='2017-7-8', freq='D')}
def check_data(data, workdate_list, show_info=False, return_data=False, show_data=False):
    """
    Description:
        Load data from orignal .csv file, select timestamp as index and use the metric-column(the last column)
        as the main data. Show some useful information of the data, and return useful data (usedata, workdate_list) if needed.
    """
    # load data, indexcol=1
    # data = pd.read_csv(filename, index_col=1)
    # change index format
    # data.index = pd.to_datetime(data.index)
    # select the last column
    if len(workdate_list) < 5:
        raise WorkDaysTooFewException("工作日天数不能少于5，当前值%s" % len(workdate_list))

    usedata = data[[data.columns[-1]]]
    if show_info:
        # calculate basic analysis result
        date_list = sorted({i.strftime("%Y-%m-%d") for i in usedata.index})
        firstday_datetime = usedata.loc[date_list[0]].index
        print date_list
        # workdate_list = sorted(
        #     {i.strftime('%Y-%m-%d') for i in pd.date_range(start=date_list[0], end=date_list[-1], freq='B')} -
        #     chinese_holiday)
        ppd = usedata.size / len(date_list)
        nan_percent = float(((pd.isnull(usedata).sum() / usedata.size)[0]))
        # evaluate use_data
        evaluate = []
        normal_date_list = []
        if ppd not in [1440, 720, 420]:
            evaluate.append('!!!Data Missing!!!')
            for i in date_list:
                print usedata[i].shape
                if usedata[i].size in [1440]:
                    normal_date_list.append(i)
                    # raise Exception("ppd = {}  not in 1440,720,420".format(ppd))
        if nan_percent > 0.50:
            evaluate.append('!!!Too many Nans!!!')
            # raise NanTooManyException("空值的百分比已经超过50%")
        print '========' * 5 + '[Data Analysis Results]' + '========' * 5
        # print 'Filename:\t', filename.split('/')[-1]
        print 'DateTime Info:\t[{0} ~ {1}] [{2}/{3} days] [{4} ~ {5}] [{6}s]'.format(date_list[0],
                                                                                     date_list[-1],
                                                                                     len(workdate_list),
                                                                                     len(date_list),
                                                                                     firstday_datetime[0].time(),
                                                                                     firstday_datetime[-1].time(),
                                                                                     (firstday_datetime[1] -
                                                                                      firstday_datetime[0]).seconds)
        print 'Data size:\t{}'.format(usedata.size)
        print 'Points Per Day:\t{}'.format(ppd)
        print 'Nan percent:\t{:2.2f}%'.format(100 * nan_percent)
        print 'Evaluate result: {}'.format(evaluate if evaluate else ['Normal Data'])
    # show usedata?00
    if show_data:
        usedata.plot(figsize=(15, 7))
        plt.show()
    # return usedata and workdate_list? 
    if return_data:
        # return pd.concat([usedata.loc[date] for date in workdate_list]), workdate_list
        return pd.concat([usedata.loc[date] for date in normal_date_list])
