from __future__ import division
import os
import numpy as np
import pickle
import matplotlib.pyplot as plt
from PyAstronomy import pyasl
from sklearn.preprocessing import normalize

__version__ = 'V3.5'


def smooth_moving(x, window_len=5):
    """smooth data uisng moving window method
    input:
        x: input data
        window_len: the dimension of the smoothing window; default is 5, and must be odd
    output:
        the smoothed data
    """
    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."

    if window_len < 3:
        return x

    if window_len % 2 == 0:
        raise ValueError, "window_len should be an odd integer."
    s = np.r_[x[window_len - 1:0:-1], x, x[-2:-window_len - 1:-1]]
    w = np.ones(window_len, 'd')
    y = np.convolve(w / w.sum(), s, mode='valid')
    return y[(window_len - 1) // 2:-(window_len - 1) // 2]


class DTA(object):
    """
    Dynamic Threshold Algorithm Class
    ppd = 420
    """

    def __init__(self, k=(1, 1), model_path='noname_model.pkl'):
        self._update_times = 0
        self.k1 = k[0]
        self.k2 = k[1]
        self._ppd = 1440
        self.model_path = model_path
        self.metric_name = model_path.split('/')[-1][:-10]
        with open(model_path, 'rb') as f:
            self.model = pickle.load(f)
        self.base_data = self.model['base_data']
        self.sigma = self.model['sigma']
        self.lines = dict(baseline=np.array([]), upper_thresh=np.array([]), lower_thresh=np.array([]))
        print self

    def __repr__(self):
        return "Dynamic Threshold Algorithm" + '(' + 'k1=' + str(self.k1) + ', k2=' + str(self.k2) + ')'

    def normalize_data(self, X):
        """
        Standardization the np.ndarray data (maybe with nan)

        Inputs:
        - data_ndarray: np.ndarray data which shape is (days x pdd)

        Outputs:
        - Standardization data the same shape with X
        """
        return ((X.T - np.nanmean(X, axis=1)) / np.nanstd(X, axis=1)).T

    def kNN(self, X, mu, n=5):
        """
        Return base_data
        n is length of base_data, default is 5
        """
        dist = ((X - mu) ** 2).sum(axis=1)
        idx_n = np.argsort(dist)[:n]
        return idx_n

    def get_params(self):
        """Get k1, k2"""
        return dict(k1=self.k1, k2=self.k2)

    def set_params(self, k1, k2):
        """Set k1, k2"""
        if k1 <= 0 or k2 <= 0:
            raise ValueError("k1, k2 must be > 0")
        else:
            self.k1 = k1
            self.k2 = k2

    def get_model(self):
        """Return the base_data and sigma of the model"""
        return dict(base_data=self.base_data, sigma=self.sigma)

    def set_model(self, model):
        """Change the model"""
        self.base_data = model['base_data']
        self.sigma = model['sigma']

    def calculate(self, refer_data=None, smooth_window_len=5):
        """
        Calculate the baseline, upper_thresh and lower_thresh

        Inputs:
        - refer_data: one day data used to polyfit the baseline, r3: default is no counter_normalization.
                      the refer data shouldn't contain more than 10% nan values
        - smooth_window_len: the degree to smooth the lines

        Outputs:
        - save the three lines in self, e.g.
          >>> A = DTA(...)
          >>> A.calculate()
          >>> A.lines['baseline'] # return baseline
          ...
        """
        max_anomaly_num = 500
        base_data_norm = normalize(np.nan_to_num(self.base_data), norm='max')
        mu_norm = np.nanmean(base_data_norm, axis=0)
        diff_data = base_data_norm - mu_norm
        r = pyasl.generalizedESD(diff_data.ravel(), max_anomaly_num, alpha=0.05, fullOutput=False)
        anomaly_index = np.array(r[1], dtype='int64')
        base_data_no_anomaly = self.base_data.copy()
        base_data_no_anomaly[np.unravel_index(anomaly_index, (5, self._ppd))] = np.nan
        mu_no_anomaly = np.nanmean(base_data_no_anomaly, axis=0)
        upper_sigma = np.nanstd((base_data_no_anomaly - mu_no_anomaly), axis=0)
        lower_sigma = np.nanstd((base_data_no_anomaly - mu_no_anomaly), axis=0)
        base_data_flat = base_data_no_anomaly
        base_data = base_data_flat.reshape((-1, self._ppd))

        upper_thresh = smooth_moving(np.nanmax(base_data, axis=0) + self.k1 * upper_sigma, window_len=smooth_window_len)
        lower_thresh = smooth_moving(np.maximum(np.nanmin(base_data, axis=0) - self.k2 * lower_sigma, 0),
                                     window_len=smooth_window_len)
        baseline = smooth_moving(np.nanmean(self.base_data, axis=0), window_len=smooth_window_len)

        # apply polyfit by jly 2018/5/15
        if refer_data is not None:
            p = np.polyfit(np.nanmean(self.base_data, axis=0), np.nan_to_num(refer_data), 1)
        baseline = baseline * p[0] + p[1]
        upper_thresh = upper_thresh * p[0] + p[1]
        lower_thresh = lower_thresh * p[0] + p[1]

        self.lines = dict(baseline=baseline, upper_thresh=upper_thresh, lower_thresh=lower_thresh)
#         print "Lines Calculated"

    def update_model(self, new_data, update_base_data=True, update_sigma=False):
        """
        Update model using new_data if new_data can be accepted by base_data, default is keeping
        updating everyday, new_data can always be the last day data. Sigma-update feature is still
        for testing, and it need not be updated to often, so default is no updating

        Inputs:
        - new_data: one day data which is ready to be updated into the base_data if it is accepted
        - update_base_data: the switch of whether to update base_data or not, default is True
        - update_base_data: the switch of whether to update sigma or not, default is False

        r3: new_data must be normalized to decided whether to update
            base_data keep normalized data
        """
        max_anomaly_num = 600
        if self._update_times == 1:
            print "You have already updated!"
            return
        if new_data.ndim == 1:
            new_data = new_data.reshape(1, new_data.size)
        if update_base_data:
            concat_data = np.concatenate((self.base_data, new_data), axis=0)
            concat_data_norm = normalize(np.nan_to_num(concat_data), norm='max')
            mu_norm = concat_data.mean(axis=0)
            diff_data = concat_data_norm - mu_norm
            r = pyasl.generalizedESD(diff_data.ravel(), max_anomaly_num, alpha=0.05, fullOutput=False)
            anomaly_index = np.array(r[1], dtype='int64')
            anomaly_num = (anomaly_index > 5 * self._ppd).sum()
            if anomaly_num < 100:
                self.base_data = concat_data[1:]
                for path, dirs, files in os.walk('.'):
                    if self.model_path in files:
                        if self.model_path + '.bak' in files:
                            os.remove(self.model_path + '.bak')
                        os.rename(path + '\\' + self.model_path, path + '\\' + self.model_path + '.bak')
                with open(self.model_path, 'wb') as f:
                    self.model['base_data'] = self.base_data
                    pickle.dump(self.model, f)
                logger.info("Update {!s} complete!".format(self.model_path))
            else:
                logger.info("No need to update {!s}!".format(self.model_path))
        self._update_times += 1

    def draw_lines(self, test_data=False, save=False, figsize=(15, 7)):
        """
        Virtualize the lines, and test_data is choosed for testing the lines, anomalies can be shown
        r3: Can save figure, and show metric name
        """
        if self.lines['baseline'].shape[0] == 0:
            print "Lines haven't been calculated!"
            return
        t = np.linspace(0, self._ppd, self._ppd)
        plt.figure(figsize=figsize)
        plt.plot(self.lines['baseline'], 'g--', label='baseline')
        plt.fill_between(t, self.lines['lower_thresh'], self.lines['upper_thresh'], color=[0.5, 0.5, 0.5, 0.5])
        if np.any(test_data):
            if test_data.ndim == 2:
                test_data = test_data.ravel()
            idx = np.where(
                np.any([test_data > self.lines['upper_thresh'], test_data < self.lines['lower_thresh']], axis=0))
            plt.plot(test_data, 'k', label='test data')
            plt.plot(idx[0], test_data[idx], 'ro', label='anomalies')
            plt.title(
                'Dynamic Threshold : k1={0}, k2={1}, anomaly num={2}. Metric: {3}'.format(self.k1, self.k2, idx[0].size,
                                                                                          self.metric_name))
        else:
            plt.title('Dynamic Threshold : k1={0}, k2={1}. Metric: {2}'.format(self.k1, self.k2, self.metric_name))
        plt.legend()
        if save:
            if not os.path.exists('figures/'):
                os.mkdir('figures/')
            plt.savefig('figures/' + self.metric_name + '.png', dpi=300)
        plt.show()
