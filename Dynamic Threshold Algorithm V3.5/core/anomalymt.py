import threading
from time import time

import pandas as pd

from dta import DTA

# import numpy as np
# import matplotlib.pyplot as plt

# import datatools as dt

"""
Anomaly Detector multi-thread version.
"""

__version__ = '1.0'


class AnomalyDetector(object):
    def __init__(self, data_frame=None):
        # self._df_list = self._tolist(data_frame)
        self._metrics = None
        self._calc = False
        self._lines = None    # dict which contains dynamic threshold
        if data_frame is not None:
            self.fit(data_frame)

    def df2cols(self, df):
        for col in df.columns:
            yield df[[col]]

    def _fit_loop(self, df):
        self._dta_dict[str(df.columns[0])] = DTA(df=df,
                                                 name=str(df.columns[0]))

    def fit(self, data_frame=None):
        start = time()
        if data_frame is not None:
            threads = []
            self._metrics = [str(i.columns[0])
                             for i in self.df2cols(data_frame)]
            self._dta_dict = {}
            nloops = range(len(self._metrics))
            # append threads
            for i in self.df2cols(data_frame):
                t = threading.Thread(target=self._fit_loop, args=(i,))
                threads.append(t)

            # start threads
            for i in nloops:
                threads[i].start()

            for i in nloops:
                threads[i].join()
        end = time()
        print('DTA classes {!r} Initialized!'.format(self._metrics))
        print('Finished in {:g}s'.format(end - start))

    def detect(self, data_frame, verbose=0, savefig=False):
        frames = []
        columns = data_frame.columns.tolist()
        for i in columns:
            if i in self._metrics:
                frames.append(self._dta_dict[i].detect(
                    data_frame[[i]], verbose=verbose, savefig=savefig))
            else:
                print(
                    'There no threshold for metric: {!s}\nSkipped!'.format(i))
        return pd.concat(frames, axis=1)

    @property
    def lines(self):
        if self._calc:
            return self._lines
        for dta in self._dta_dict.values():
            dta.calculate()
        self._calc = True
        self._lines = {i: self._dta_dict[i].lines for i in self._metrics}
        return {i: self._dta_dict[i].lines for i in self._metrics}

    def adujust(self, kwargs):
        """
        'kwargs' contains the adjust parameters of each metric.
        Format like this:
            ('TRADE', +0.5)
            ('FINANCE', -0.5)
            ('SHOUYE', 0)
        '+': adjust up
        '-': adjust down
        '0': reset to original
        """
        if self._calc:
            for metric, k in kwargs.iteritems():
                print(metric, k)
                if k > 0:
                    self._dta_dict[metric].adjust(method='upper', degree=k)
                elif k < 0:
                    self._dta_dict[metric].adjust(method='lower', degree=-k)
                else:
                    self._dta_dict[metric].adjust(method='auto', degree=0)
            self._lines = {i: self._dta_dict[i].lines for i in self._metrics}
        else:
            raise Exception('Lines to be calculated!')

    def ialert(self, data_frame, kwargs):
        """
        isolation-metric alert
        'kwargs' contains the alert parameters of each metric.
        Format like this:
            ('TRADE', 3)
            ('FINANCE', 2)
            ('SHOUYE', 5)
        """
        anomaly_pool = self.detect(data_frame)
        alert_dict = {}
        for metric, k in kwargs.iteritems():
            if k <= 0:
                raise Exception('Invalid k, must be >=1!')
            alert = anomaly_pool[[metric]].rolling(
                k).sum().fillna(method='bfill')
            alert_dict[metric] = alert.loc[(alert >= k).any(
                axis=1) is True].index.tolist()
        return alert_dict

    def malert(self, data_frame, k):
        """
        multi-metric alert
        k is the common parameter for related metrics
        """
        anomaly_pool = self.detect(data_frame)
        alert = anomaly_pool.rolling(k).sum().fillna(method='bfill')
        return alert.loc[(alert >= k).any(axis=1) is True].index.tolist()
