"""
Dynamic Threshold Algorithm 4
Author: jly
"""
from __future__ import division, print_function

import os
import pickle
from collections import namedtuple
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PyAstronomy.pyasl.asl.outlier import generalizedESD
from sklearn import svm
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor
from statsmodels.tsa.seasonal import seasonal_decompose

import datatools as dt

# version info
__version__ = '4.0.1'

# define nametuples: Lines (Dynamic Threshold), K (basic parameters), tsr(model)
Lines = namedtuple('Lines', ['baseline', 'lower_thresh', 'upper_thresh'])
K = namedtuple('K', ['kl', 'ku', 'kn'])
TSR = namedtuple('TSR', ['trend', 'seasonal', 'residual'])


def adjust_up(x, k):
    """
    Adjust upper.
    """
    return (1 + k * x / x.max()) * x


def adjust_low(x, k):
    """
    Adjust lower.
    """
    return (1 - k * x / x.max()) * x


def wash(data, method='ocs'):
    """
    Wash data first
    Method: ocs: OneClassSVM
            ifrt: IsolationForest
            lof: LocalOutlierFactor
    """
    data_re = data.reshape((-1, 1440))
    data_re_norm = dt.normalize_max(data_re)
    if method == 'lof':
        clf = LocalOutlierFactor()
        labels = clf.fit_predict(data_re_norm)
    elif method in ['ocs', 'ifrt']:
        if method == 'ocs':
            clf = svm.OneClassSVM(kernel='rbf').fit(data_re_norm)
        else:
            clf = IsolationForest().fit(data_re_norm)
        labels = clf.predict(data_re_norm)
    else:
        raise Exception(
            "Wrong method: {!r}. Choose from ['ocs', 'ifrt', 'lof']".format(method))
    return data_re[np.where(labels == 1)]


def tsr_decompose(data):
    """Decompose data."""
    result = seasonal_decompose(data.ravel(), model='additive', freq=1440)
    trend = result.trend.reshape((-1, 1440))[1:-1]
    seasonal = result.seasonal[-1440:]
    residual = noise_model(result.resid)
    return TSR(trend, seasonal, residual)


def predict_baseline(model, sm=1):
    """Predict baseline."""
    trend, seasonal, _ = model
    n = trend.shape[0]
    x = np.linspace(0, n - 1, n)[:, np.newaxis]
    clf = svm.SVR().fit(x, trend.mean(axis=1))
    y = clf.predict(n)
    baseline = smooth(y + seasonal, window_len=sm)
    return baseline


def noise_model(noise):
    """Build noise model."""
    noise_re = np.absolute(noise.reshape((-1, 1440)))
    # build gussian model
    mu, std = np.nanmean(noise_re, axis=0), np.nanstd(noise_re, axis=0)
    return mu, std


def gen_noise(mu_std, random_state=None):
    """Generate noise."""
    mu, std = mu_std
    np.random.seed(random_state)
    return np.random.normal(mu, std)


def calc_dynamic_thresh(baseline, min_thresh, model, k=(0, 0, 0), sm=1, lower_bounds=0, upper_bounds=None):
    """Calculate dynamic thresh"""
    kl, ku, kn = k
    # random generate noise
    lower_noise = gen_noise(
        model.residual, random_state=np.random.randint(100))
    upper_noise = gen_noise(
        model.residual, random_state=np.random.randint(100))
    # basic threshold
    lower_thresh_base = smooth(baseline - min_thresh, window_len=sm)
    upper_thresh_base = smooth(baseline + min_thresh, window_len=sm)
    # adjust threshold and add noise
    lower_thresh = smooth(adjust_low(
        lower_thresh_base, kl) - kn * np.maximum(lower_noise, 0))
    upper_thresh = smooth(adjust_up(upper_thresh_base, ku) +
                          kn * np.maximum(upper_noise, 0))
    # lower_thresh must be >= lower_bounds, default is 0
    lower_thresh = np.maximum(lower_thresh, lower_bounds)
    # upper_thresh must be <= upper_bounds, default is None
    if upper_bounds:
        upper_thresh = np.minimum(upper_thresh, upper_bounds)
    return np.array(lower_thresh, dtype='float64'), np.array(upper_thresh, dtype='float64')


def smooth(x, window_len=11, window='hanning'):
    """Smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal 
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")
    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")
    if window_len < 3:
        return x
    if window not in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError(
            "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")
    s = np.r_[x[window_len - 1:0:-1], x, x[-2:-window_len - 1:-1]]
    if window == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('np.' + window + '(window_len)')
    y = np.convolve(w / w.sum(), s, mode='valid')
    return y[(window_len - 1) // 2:-(window_len - 1) // 2]


class DTA(object):
    """DTA class: Dynamic Threshold Algorithm with outlier detection methods."""

    def __init__(self, df=None, name=None, holiday_list=[]):
        self._default_k = K(0.2, 0.3, 1)
        self._k = self._default_k
        self._sm = 5
        self._name = None
        self._model = None
        self._lines = None

        if name:
            self._name = name
        if df is not None:
            self.build(df, holiday_list=holiday_list)

    def __repr__(self):
        return 'DTA<{!s}>'.format(self._name)

    def _check_model(self):
        """Check whether instance contains model"""
        if self._model is None:
            print("Model is empty! You must build model first.")

    def _check_lines(self):
        """Check whether lines have been calculated"""
        return not self._lines is None

    def set_default_k(self, k):
        """Set default k"""
        self._default_k = K(k[0], k[1], k[2])

    def get_default_k(self):
        """Get default k"""
        return self._default_k

    def setk(self, k):
        """Set k"""
        self._k = K(k[0], k[1], k[2])

    def getk(self):
        """Return K true value"""
        return self._k

    def setsm(self, sm):
        """Set smooth window length"""
        self._sm = sm

    def getsm(self):
        """Get smooth window length"""
        return self._sm

    def exist_model(self):
        return not self._model is None

    @property
    def k(self):
        """Show k values"""
        return 'kl:{0:g} ku:{1:g} kn:{2:g}'.format(*self._k)

    @property
    def lines(self):
        """Return lines which contains: baseline, lower_thresh, upper_thresh"""
        if self._check_lines():
            return self._lines
        else:
            return None

    def build(self, df, holiday_list=[]):
        """Extract trend, seasonal and residual information from historical series."""
        # filter data, max_nan = 0.01
        data = dt.preprocess(df, holiday_list=holiday_list, max_nan=0.01)
        # choose normal-day data
        data_nd = wash(data.values, method='ifrt')
        if data_nd.shape[0] < 5:
            print('Please provide more than 5 days data!')
        else:
            # perform tsr_decompose
            self._model = tsr_decompose(data_nd)
            print('Model built: {!s}'.format(self._name))

    def calculate(self, min_thresh=None, lower_bounds=None, upper_bounds=None):
        """Calculate dynamic threshold which contains three lines: baseline, lower_thresh, upper_thresh"""
        # step 1: check model
        self._check_model()
        # step 2: initialize basic parameters
        if min_thresh is None:
            min_thresh = np.max(self._model.seasonal) / 40
        # step 3: predict baseline
        baseline = predict_baseline(self._model)
        # step 4: calculate dynamic threshold
        lower_thresh, upper_thresh = calc_dynamic_thresh(
            baseline, min_thresh, self._model, self._k, self._sm, lower_bounds, upper_bounds)
        # step 5: package the lines, save to self.lines and return
        self._lines = Lines(baseline, lower_thresh, upper_thresh)
        print('Lines calculated!')

    def detect(self, data, verbose=0, savefig=False, name=None):
        """Use the dynamic threshold and outlier detection algorithm to detect anomalies"""
        # step 1: check model and lines
        self._check_model()
        if not self._check_lines():
            print('Lines haven\'t been calculated! Auto calculate!')
            self.calculate()
        data = dt.fillnan(data).interpolate(limit_direction='both')
        metric = str(data.columns[0])
        date_list = dt.get_date_list(data)
        anomaly_frames = []
        for date in date_list:
            print('Detecting {0}: {1} ... '.format(metric, date))
            test_data = data.loc[date].values.copy()
            # anomaly breakthrough the dynamic threshold
            anomaly_idx0 = np.where(np.any([test_data < self._lines.lower_thresh[:, np.newaxis],
                                            test_data > self._lines.upper_thresh[:, np.newaxis]], axis=0))[0]

            # anomaly detected by generalized ESD
            anomaly_idx1 = np.array(generalizedESD(test_data - self._lines.baseline[:, np.newaxis],
                                                   maxOLs=np.maximum(anomaly_idx0.size, 1))[1], dtype='int64')
            # maybe add more anomaly detectors which return anomaly index of numpy.ndarray format
            # anomaly_idx2
            # ... ...
            # anomaly_idxn

            # generate anomaly_pool
            anomaly_idx = np.intersect1d(anomaly_idx0, anomaly_idx1)
            anomaly_frame = np.zeros((test_data.size, 1))
            anomaly_frame[anomaly_idx] += 1
            anomaly_frames.append(dt.generate_df(
                anomaly_frame, date, [self._name]))
            # for i in range(n):
            #     anomaly_pool[anomaly_idxi] += 1
            # anomaly_pool /= n
            # anomaly_pool[anomaly_idx1] += 0.1

            if verbose == 1:
                t = np.linspace(0, 1439, 1440)
                fig = plt.figure(figsize=(15, 7))
                ax1 = fig.add_subplot(111)
                ax2 = fig.add_subplot(111, sharex=ax1, frameon=False)
                ax2.yaxis.tick_right()
                ax1.set_ylabel('Test Data')
                ax2.set_ylabel('Anamoly Score')
                ax2.set_ylim([-0.01, 1.1])
                ax2.yaxis.set_label_position("right")
                ax1.plot(self._lines.baseline, 'g--', label='baseline')
                ax1.fill_between(t, self._lines.lower_thresh, self._lines.upper_thresh,
                                 color=[.5, .5, .5], alpha=.5, label='Dynamic Threshold')
                ax1.plot(test_data, 'b', label='test data')
                # plt.plot(anomaly_idx1, test_data[anomaly_idx1], 'mD', label='generalizedESD')
                ax2.plot(anomaly_frame, 'r', label='anomaly', alpha=0.5)
                # plt.plot(test_data - self._lines.baseline, label='Residual')
                plt.title('DTA4.0: {0}: {1}'.format(self._name, date))
                ax1.legend(loc=2)
                ax2.legend(loc=1)
                if savefig:
                    if name:
                        plt.savefig(
                            'DTA4.0 {!s}.png'.format(name), dpi=300)

                    plt.savefig(
                        'DTA4.0 Demo({0}-{1}).png'.format(metric, date), dpi=300)
                plt.show()
        return pd.concat(anomaly_frames, axis=0)

    def update(self, df=None, holiday_list=[]):
        """Build the model use new data"""
        self._model = self.build(df, holiday_list=holiday_list)
        print('Model updated at {!s}!'.format(
            datetime.now().strftime('%Y-%m-%d %H:%M:%S')))

    def save(self, directory='.'):
        """Save the model to the directory"""
        try:
            path = os.path.join(directory, self._name)
            if not os.path.isdir(path):
                os.makedirs(path)
            files = os.listdir(path)
            # try to keep the lastest 5 model files
            if len(files) > 4:
                os.remove(os.path.join(path, min(files)))
            fullpath = os.path.join(directory, self._name, str(
                datetime.now().strftime('%Y%m%d_%H%M%S')) + '.pkl')
            with open(fullpath, 'a+') as f:
                pickle.dump(self._model, f)
            print('Model {!s} saved at {!s}'.format(self, fullpath))
        except Exception:
            raise Exception('Failed to save model: {!r}!'.format(self._name))

    def restore(self, directory='.'):
        """Restore the model"""
        try:
            path = os.path.join(directory, self._name)
            if not os.path.isdir(path):
                raise Exception('No such directory!')
            files = os.listdir(path)
            fullpath = os.path.join(path, max(files))
            with open(fullpath) as f:
                self._model = pickle.load(f)
            print('Model {!s} restored to {!s}'.format(fullpath, self))
        except Exception:
            raise Exception('Failed to restore model: {!r}!')

    def adjust(self, method='reset', degree=0.1):
        """"Adjust k and lines"""
        if method not in ['reset', 'wider', 'narrower', 'upper', 'lower']:
            raise Exception('No such method {!r}'.format(method))
        if degree < 0 or degree > 1:
            raise Exception('Degree must be >= 0 and <=1!')
        # to adjust
        if method == 'wider':
            self.setk(((1 + degree) * self._k.kl,
                       (1 + degree) * self._k.ku,
                       self._k.kn))
            self.calculate()
        elif method == 'narrower':
            self.setk(((1 - degree) * self._k.kl,
                       (1 - degree) * self._k.ku,
                       self._k.kn))
            self.calculate()
        elif method == 'upper':
            self._lines = Lines(adjust_up(self._lines.baseline, degree),
                                adjust_up(self._lines.lower_thresh, degree),
                                adjust_up(self._lines.upper_thresh, degree))
        elif method == 'lower':
            self._lines = Lines(adjust_low(self._lines.baseline, degree),
                                adjust_low(self._lines.lower_thresh, degree),
                                adjust_low(self._lines.upper_thresh, degree))
        else:
            self._k = self._default_k
            self.calculate()
