# -*- coding: utf-8 -*-
from __future__ import print_function

import math
import socket
from time import sleep
import pandas as pd
import re

import requests


def ping(host, port):
    """Ping http://host:port.

    Arguments:
        host {str} -- host ip
        port {int} -- port

    Returns:
        bool -- whether connection has been built
    """

    try:
        socket.socket().connect((host, port))
        print('Ping {!s}:{!s} ... OpenTSDB connection status: OK'.format(
            host, port))
        return True
    except socket.error as err:
        if err.errno == socket.errno.ECONNREFUSED:
            raise Exception('Can\'t connect to OpenTSDB Server')
        raise Exception('Fail to test OpenTSDB connection status')


class Connection(object):
    """Class of Connection to OpenTSDB.
    """

    # unit argument for pd.to_datetime
    __unit_table = {10: 's', 13: 'ms', 16: 'ns'}

    def __init__(self, server='localhost', port=4242):
        self.server = server
        self.port = port
        # test the OpenTSDB connection status
        ping(server, port)
        self.url = 'http://{!s}:{:d}'.format(server, port)

    @staticmethod
    def __metric_tags(str_data):
        try:
            metric = str_data.split('{')[0]
            tag_list = re.search(r'{.*}', str_data).group(0)[1:-1]
            tags = dict(tuple(i.split('=')) for i in tag_list.split(', '))
            return metric, tags
        except Exception:
            raise Exception("Can't write data without tags!")

    @staticmethod
    def __format_tags(tags):
        return '{' + ', '.join(str(k) + '=' + str(v)
                               for (k, v) in tags.iteritems()) + '}'

    @staticmethod
    def __dict2df(json_data, metric, tags, timedelta=+8):
        timestamps, data = zip(*sorted(json_data.items()))
        try:
            unit = Connection.__unit_table[len(str(timestamps[0]))]
        except Exception:
            print(len(str(timestamps[0])))
            raise Exception('Timestamp Error!')
        return pd.DataFrame(list(data),
                            index=pd.to_datetime(timestamps, unit=unit) +
                            pd.Timedelta('{!r} h'.format(timedelta)),
                            columns=[metric + tags],
                            dtype='f8')

    def _read(self, json_data):
        """Use post method to query for data.

        Arguments:
            json_data {dict} -- the json-encoded content for querying data

        Returns:
            dict -- the json-encoded content of a response, if any
        """

        r = requests.post(self.url + '/api/query', json=json_data)
        # check the response status
        r.raise_for_status()
        return r.json()

    def _write(self, json_data):
        """Use post method to query for data

        Arguments:
            json_data {dict} -- the json-encoded content for putting data

        Returns:
            dict -- the json-encoded content of a response, if any
        """

        r = requests.post(self.url + '/api/put?summary', json=json_data)
        # check the response status
        # r.raise_for_status()
        return r.json()

    def read_df(self, json_data):
        """Read dataframe from OpenTSDB

        Arguments:
            json_data {dict} -- query request for reading data

        Returns:
            pd.DataFrame -- the dataframe
        """

        results = self._read(json_data)
        df = pd.concat([self.__dict2df(json_data=i['dps'],
                                       metric=i['metric'],
                                       tags=self.__format_tags(i['tags']))
                        for i in results], axis=1)
        df.index.name = 'Time'
        return df

    def write_df(self, df, n=10, timedelta=+8):
        """Write dataframe to OpenTSDB.

        Arguments:
            df {pd.DataFrame} -- the dataframe of column data

        Returns:
            dict -- write results {failed: *, success: *}
        """

        json_list = []
        results = dict(failed=0, success=0)
        counter = 0
        for i in df.index:
            for j in df.columns:
                timestamp = (pd.Timestamp(i) - pd.Timedelta(
                    '{!r} h'.format(timedelta))).value // 10**9
                metric, tags = self.__metric_tags(j)
                # for debuging
                try:
                    if pd.isnull(df.loc[i, j]) or df.loc[i, j] == 'undefined':
                        continue
                except:
                    print(i, j, df.loc[i, j], type(df.loc[i, j]))
                json_data = dict(
                    metric=metric,
                    timestamp=int(timestamp),
                    value=float(df.loc[i, j]),
                    tags=tags)
                json_list.append(json_data)
                if len(json_list) == n:
                    result = self._write(json_list)
                    results['failed'] += result['failed']
                    results['success'] += result['success']
                    json_list = []
                    print('.', end='')
                    counter += 1
                    if counter == 80:
                        counter = 0
                        print('\r')
                    sleep(0.05)
        result = self._write(json_list)
        results['failed'] += result['failed']
        results['success'] += result['success']
        print('.', end='')
        print('\nFinished! Results:{!r}\n'.format(results))
        return results

    # reserved read_ts interface
    def read_ts(self, json_data):
        """Reserved interface: read_ts.

        Arguments:
            json_data {dict} -- json-encoded content
        """

        self.read_df(json_data)

    # reserved write_ts interface
    def write_ts(self, df, n=10):
        """Reserved interface: write_ts.

        Arguments:
            df {pd.DataFrame} -- the dataframe of column data

        Keyword Arguments:
            n {int} -- number of json to post at one time (default: {10})
        """

        self.write_df(df, n)
