# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from PyAstronomy import pyasl
import datatools as dt


data = pd.read_csv('test1.csv', index_col=0, sep=';', skiprows=1)
data.index = pd.to_datetime(data.index) + pd.Timedelta('8h')
data = dt.preprocess(data)
# print(data.head())
# plt.figure(figsize=(15, 8))
# plt.plot(data)
# plt.show()

k1, k2 = 3, 3
date_list = dt.get_date_list(data)
columns = data.columns
ppd = 1440
smooth_len = 5
anomaly_num = 100
for i in range(len(columns)):
    for j in range(len(date_list)):
        base_data_flat = data[columns[i]].values.copy()  # must have copy

        base_data = base_data_flat.reshape((-1, ppd))
        days = len(date_list)
        base_data_norm = dt.normalize_max(base_data)
        mu = base_data.mean(axis=0)
        mu_norm = base_data_norm.mean(axis=0)

        diff_data = base_data_norm - mu_norm
        r = pyasl.generalizedESD(
            diff_data.ravel(), anomaly_num, alpha=0.05, fullOutput=False)
        anomaly_index = np.array(r[1], dtype='int64')
        # 1. consider anoamlies affect
        # base_data_flat[anomoly_index] = mu[anomoly_index % ppd]
        # upper_sigma = (base_data - mu).std(axis=0)
        # lower_sigma = (base_data - mu).std(axis=0)
        # 2. drop anomalies
        base_data_no_anomaly = base_data.copy()
        base_data_no_anomaly[np.unravel_index(
            anomaly_index, (days, ppd))] = np.nan
        mu_no_anomaly = np.nanmean(base_data_no_anomaly, axis=0)
        upper_sigma = np.nanstd((base_data_no_anomaly - mu_no_anomaly), axis=0)
        lower_sigma = np.nanstd((base_data_no_anomaly - mu_no_anomaly), axis=0)
        base_data_flat = base_data_no_anomaly
        base_data = base_data_flat.reshape((-1, ppd))
        upper_thresh = dt.smooth_moving(
            np.nanmax(base_data, axis=0) + k1 * upper_sigma, window_len=smooth_len)
        lower_thresh = dt.smooth_moving(np.maximum(
            np.nanmin(base_data, axis=0) - k2 * lower_sigma, 0), window_len=smooth_len)
        # real test
        # for date in date_list[:2]:
        # real_test_data = test_data.loc[date, 'SHOUYE'].copy()
        idx = np.where(np.any([data[columns[i]].loc[date_list[j]].values > upper_thresh, data[columns[i]].loc[date_list[j]].values < lower_thresh], axis=0))
        plt.figure(figsize=(15, 5))
        # real_test_data.plot(figsize=(15, 8))
        # t = real_test_data.index.values
        t = np.linspace(0, 1439, 1440)
        plt.plot(t[idx[0]], data[columns[i]].loc[date_list[j]].values[idx[0]], 'ro')
        plt.plot(data[columns[i]].loc[date_list[j]].values)
        plt.fill_between(t, lower_thresh, upper_thresh,
                         color=[0.5, 0.5, 0.5], alpha=0.5)
        # plt.plot(lower_thresh)
        # plt.plot(upper_thresh)
        # plt.plot(data)
        plt.title('DTA3: {!s} {!s}'.format(columns[i], date_list[j]))
        plt.savefig('DTA3_{!s}_{!s}.png'.format(columns[i], date_list[j]))
        plt.show()
