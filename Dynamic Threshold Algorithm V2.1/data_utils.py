import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.externals import joblib


def load_csv_data(filename, ppd=720):
    """
    Load the csv data.

    Inputs:
    - filename: The name of the csv file
    - ppd: Points Per Day, default 720 (from 9:00 to 15:00, step is 30s)

    Outputs:
    - data: DataFrame format data with its DatetimeIndex that used to plot
    - data_ndarray: The np.ndarray format data which shap is (days x ppd)
    """
    data = pd.read_csv(filename, index_col=0)
    data.index = pd.to_datetime(data.index)
    days = data.shape[0] / ppd
    data_ndarray = np.reshape(data.values, (days, ppd))
    return data, data_ndarray


def normalize_data(data_ndarray):
    """
    Standardization the np.ndarray data

    Inputs:
    - data_ndarray: np.ndarray data which shape is (days x pdd)

    Outputs:
    - Standardization data the same shape with data_ndarray
    """
    return ((data_ndarray.T - data_ndarray.mean(axis=1)) / data_ndarray.std(axis=1)).T


def normalize_load(filename, ppd=720):
    """
    Load csv data and return standardization data
    """
    _, data_ndarray = load_csv_data(filename)
    return normalize_data(data_ndarray)


def save_train_test(metric, array, labels, test_size, random_state=42, return_data=False):
    """
    Split the labled data into training set and test set
    The save them in the local directory

    Inputs:
    - metric: The metric_name
    - array: The ndarray data (days x ppd)
    - labels: The labels of the data (days, )
    - test_size: Size of test data
    - random_state: 
    - return_data: Boolean values, True means return training and test data, default is False

    Outputs:
    * Generate two files
    * The last element of each row is the label
    - (train, test, train_labels, test_labels): 
    """
    train, test, train_labels, test_labels = train_test_split(array,
                                                              labels,
                                                              test_size=test_size,
                                                              random_state=random_state)
    np.save(metric + '_training', np.append(train,
                                            np.reshape(train_labels, (train_labels.shape[0], 1)), axis=1))
    np.save(metric + '_test', np.append(test,
                                        np.reshape(test_labels, (test_labels.shape[0], 1)), axis=1))
    if return_data:
        return train, test, train_labels, test_labels


def load_train_test(train_filename, test_filename):
    """
    Load the train, test with their labels
    """
    train_data = np.load(train_filename)
    test_data = np.load(test_filename)
    train, train_labels = np.hsplit(
        train_data, np.array([train_data[0].shape[0] - 1]))
    test, test_labels = np.hsplit(
        test_data, np.array([test_data[0].shape[0] - 1]))
    return train, test, train_labels.ravel(), test_labels.ravel()


def save_gnb_model(metric, train, test, train_labels, test_labels, return_data=False):
    """
    Generate Gaussian Naive Bayes model of the training set and save the model
    Can also return model if you want

    Inputs:
    - metric: The name of metric
    - train:
    - test:
    - train_labels:
    - test_labels
    - return_data: Boolean values, True means return the model, default is False

    Outputs:
    """
    gnb = GaussianNB()
    model = gnb.fit(train, train_labels)
    preds = gnb.predict(test)
    joblib.dump(gnb, metric + '_gnb_model.pkl')
    print(accuracy_score(test_labels, preds))
    if return_data:
        return model


def load_model(model_path):
    """Load the model (.pkl)"""
    return joblib.load(model_path)


def split_data(array, model):
    """
    Split data into two groups: the positive data and the negative data 

    Inputs:
    - array: Data to by split (days x ppd)
    - model: The trained model that can predict correctly

    Outputs:
    - (positive data, negative data)
    """
    preds = model.predict(array)
    return array[np.where(preds == 1)], array[np.where(preds == 0)]


def update_base_data(array, base_data, model):
    """
    Update the base_data

    Inputs:
    - array: The whole day data to be decided whether to be updated into base_data
    - base_data: The most recent data used to calculate baseline
    - model: The GNB model to predict

    Outputs:
    - New base_data
    """
    if array.ndim == 1:
        array = array.reshape(1, array.size)
    preds = model.predict(array)
    if preds[0] == 1:
        return np.append(base_data[:4], array, axis=0)
    else:
        return base_data


def smooth_moving(x, window_len=5):
    """smooth data uisng moving window method
    input:
        x: input data
        window_len: the dimension of the smoothing window; default is 5, and must be odd
    output:
        the smoothed data
    """
    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."

    if window_len < 3:
        return x

    if window_len % 2 == 0:
        raise ValueError, "window_len should be an odd integer."
    s = np.r_[x[window_len - 1:0:-1], x, x[-2:-window_len - 1:-1]]
    w = np.ones(window_len, 'd')
    y = np.convolve(w / w.sum(), s, mode='valid')
    return y[(window_len - 1) / 2:-(window_len - 1) / 2]
