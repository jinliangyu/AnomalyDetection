import numpy as np


def smooth_moving(x, window_len=5):
    """smooth data uisng moving window method
    input:
        x: input data
        window_len: the dimension of the smoothing window; default is 5, and must be odd
    output:
        the smoothed data
    """
    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."

    if window_len < 3:
        return x
    
    if window_len % 2 == 0:
        raise ValueError, "window_len should be an odd integer."
    s = np.r_[x[window_len-1:0:-1], x, x[-2:-window_len-1:-1]]
    w = np.ones(window_len, 'd')
    y = np.convolve(w/w.sum(), s, mode='valid')
    return y[(window_len-1)/2:-(window_len-1)/2]