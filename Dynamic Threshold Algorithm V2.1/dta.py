import os
import numpy as np
import pickle
import matplotlib.pyplot as plt
from smooth import smooth_moving

class DTA(object):
    """
    Dynamic Threshold Algorithm Class
    ppd = 420
    """
    def __init__(self, k=(1, 1), model_path='model.pkl'):
        self._update_times = 0
        self.k1 = k[0]
        self.k2 = k[1]
        self._ppd = 420
        self.model_path = model_path
        with open(model_path, 'rb') as f:
            self.model = pickle.load(f)
        self.base_data = self.model['base_data']
        self.sigma = self.model['sigma']
        self.lines = dict(baseline=np.array([]), upper_thresh=np.array([]), lower_thresh=np.array([]))
        print self
    
    def __repr__(self):
        return "Dynamic Threshold Algorithm"+'('+'k1='+str(self.k1)+', k2='+str(self.k2)+')'

    def kNN(X, mu, n=5):
        """
        Return base_data
        n is length of base_data, default is 5
        """
        dist = ((X - mu)**2).sum(axis=1)
        idx_n = np.argsort(dist)[:n]
        return idx_n

    def get_params(self):
        return dict(k1=self.k1, k2=self.k2)
    
    def set_params(self, k1, k2):
        if k1 <= 0 or k2 <=0:
            raise ValueError ("k1, k2 must be > 0")
        else:
            self.k1 = k1
            self.k2 = k2

    def get_model(self):
        return dict(base_data=self.base_data, sigma=self.sigma)
    
    def set_model(self, model):
        self.base_data = model['base_data']
        self.sigma = model['sigma']

    def calulate(self, refer_data, smooth_window_len=5):
        baseline = smooth_moving(self.base_data.mean(axis=0), window_len=smooth_window_len)
        if (np.isnan(refer_data)).sum() < refer_data.size / 10:
            self.p = np.polyfit(self.base_data.mean(axis=0), np.nan_to_num(refer_data), 1)
            baseline = baseline*self.p[0] + self.p[1]
        upper_thresh = smooth_moving(baseline + self.k1*self.sigma, window_len=smooth_window_len)
        lower_thresh = smooth_moving(baseline - self.k2*self.sigma, window_len=smooth_window_len)
        self.lines = dict(baseline=baseline, upper_thresh=upper_thresh, lower_thresh=lower_thresh)
        print "Lines Calculated"
    
    def update_model(self, new_data, update_base_data=True, update_sigma=False):
        if self._update_times == 1:
            print "You have already updated!"
            return
        if new_data.ndim == 1:
            new_data = new_data.reshape(1, new_data.size)
        mu = self.base_data.mean(axis=0)
        if update_base_data:
            concat_data = np.concatenate((self.base_data, new_data), axis=0)
            idx = kNN(concat_data, mu)
            if 5 in idx:
                self.base_data = concat_data[idx]
                for path, dirs, files in os.walk('.'):
                    if self.model_path in files:
                        if self.model_path+'.bak' in files:
                            os.remove(self.model_path+'.bak')
                        os.rename(path+'\\'+self.model_path, path+'\\'+self.model_path+'.bak')
                with open(self.model_path, 'wb') as f:
                    self.model['base_data'] = self.base_data
                    pickle.dump(self.model, f)
                print "Update complete!"
            else:
                print "No need to update!"
        if update_sigma:
            self.sigma = (1-0.1)*self.sigma + 0.1*(((refer_data-mu)**2).sum(axis=1))**0.5
            for path, dirs, files in os.walk('.'):
                if self.model_path in files:
                    if self.model_path+'.bak' in files:
                        os.remove(self.model_path+'.bak')
                    os.rename(path+'\\'+self.model_path, path+'\\'+self.model_path+'.bak')
            with open(self.model_path, 'wb') as f:
                self.model['sigma'] = self.sigma
                pickle.dump(self.model, f)
        self._update_times += 1
    
    def draw_lines(self, test_data=False, anomaly_on=False, figsize=(15, 7)):
        if test_data.ndim == 2:
            test_data = test_data.ravel()
        if self.lines['baseline'].shape[0] == 0:
            print "Lines haven't been calculated!"
            return
        idx = np.where(np.any([test_data>self.lines['upper_thresh'], test_data<self.lines['lower_thresh']], axis=0))
        t = np.linspace(0, self._ppd, self._ppd)
        plt.figure(figsize=figsize)
        plt.plot(self.lines['baseline'], 'g--', label='baseline')
        plt.fill_between(t, self.lines['lower_thresh'], self.lines['upper_thresh'], color=[0.5, 0.5, 0.5, 0.5])
        if np.all(test_data):
            plt.plot(test_data, 'k', label='test data')
            if anomaly_on:
                plt.plot(idx[0], test_data[idx], 'ro', label='anomalies')
        plt.legend()
        plt.title('Dynamic Threshold : k1={0}, k2={1}, anomaly num={2}'.format(self.k1, self.k2, idx[0].size))
        plt.show()
    