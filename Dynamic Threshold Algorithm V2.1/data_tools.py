import numpy as np
import pandas as pd
import pickle


def filter_data(data, history_date, rest_date, trading_time=('09:00:00', '14:59:59')):
    """
    pass
    """
    # choose the trading date
    date_index = pd.date_range(history_date[0], history_date[1], freq='D')
    date_list = [date_index[i].strftime('%Y-%m-%d')
                 for i in range(len(date_index))]
    trading_date = sorted(list(set(date_list) - set(rest_date)))

    # filter the data first
    filtered_data = pd.concat([data.loc[pd.to_datetime(date_index[i].strftime('%Y-%m-%d') + ' ' + trading_time[0])                                        :pd.to_datetime(date_index[i].strftime('%Y-%m-%d') + ' ' + trading_time[1])] for i in range(date_index.shape[0])])

    # concat workdays to new dateframe and return
    return pd.concat([filtered_data.loc[i] for i in trading_date])


def data_preprocess(X, history_date, rest_date, trading_time=('09:00:00', '14:59:59')):
    """
    Extract the trading_time data of workdays
    """
    hostnames = X.keys()
    return {hostname: filter_data(X[hostname], history_date, rest_date, trading_time=('09:00:00', '14:59:59')) for hostname in hostnames}


def normalize_data(data_ndarray):
    """
    Standardization the np.ndarray data

    Inputs:
    - data_ndarray: np.ndarray data which shape is (days x pdd)

    Outputs:
    - Standardization data the same shape with data_ndarray
    """
    return ((data_ndarray.T - data_ndarray.mean(axis=1)) / data_ndarray.std(axis=1)).T


def to_reshaped_ndarray(X):
    """
    Convert dict's values from pd.DateFrame to ndarray with shape (days x ppd)
    ppd means Points Per Day
    """
    hostnames = X.keys()
    date_list = sorted(
        list(set(X[hostnames[0]].index.strftime('%Y-%m-%d').astype('str'))))
    days = len(date_list)
    ppd = X[hostnames[0]].shape[0] / days
    return {hostname: X[hostname].values.reshape(days, ppd) for hostname in hostnames}


def drop_nan(X):
    """
    Return the X without nan
    """
    hostnames = X.keys()
    return {hostname: np.delete(X[hostname], np.where(np.any(np.isnan(X[hostname]), axis=1)), axis=0) for hostname in hostnames}


def calc_mu(X):
    """
    calculate mu
    """
    hostnames = X.keys()
    return {hostname: X[hostname].mean(axis=0) for hostname in hostnames}


def calc_sigma(X):
    """
    calculate sigma
    Here the sigma is doubled
    """
    hostnames = X.keys()
    return {hostname: 2 * X[hostname].std(axis=0) for hostname in hostnames}


def calc_mu_sigma(X):
    """
    return tuple: (mu, sigma)
    """
    return calc_mu(X), calc_sigma(X)


def kNN(X, mu, n=5):
    """
    Return base_data
    n is length of base_data, default is 5
    """
    hostnames = X.keys()
    ret_dict = {}
    for hostname in hostnames:
        dist = ((X[hostname] - mu[hostname])**2).sum(axis=1)
        idx_n = np.argsort(dist)[:n]
        ret_dict[hostname] = X[hostname][idx_n]
    return ret_dict


def pack_model(metric_name, base_data, sigma, return_model=False):
    """
    Pack base_data and sigma into a file named 'metric_name_model.pkl'
    
    Inputs:
    - metric_name: The str of metric name
    - base_data: Dict of base_data
    - sigma: Dict of sigma
    
    Outputs:
    - Save the model (.pkl) in the local directory. Return the model if you want
    """
    model = dict(base_data=base_data, sigma=sigma)
    with open(metric_name+'_model.pkl', 'wb') as f:
        pickle.dump(model, f)
    if return_model:
        return model

    
def unpack_model(filepath):
    """
    Unpack model (.pkl) to base_data and sigma
    
    Inputs:
    - filepath: Where the model file is
    
    Outputs:
    - A tuple: (base_data, sigma)
    """
    with open(filepath, 'rb') as f:
        model = pickle.load(f)
    return model['base_data'], model['sigma']