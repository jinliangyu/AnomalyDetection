from PyAstronomy import pyasl
from sklearn.preprocessing import normalize


def calulate(self, refer_data=False, smooth_window_len=5):
    """
    Calulate the baseline, upper_thresh and lower_thresh

    Inputs:
    - refer_data: one day data used to polyfit the baseline, r3: default is no counter_normalization.
                  the refer data shouldn't contain more than 10% nan values
    - smooth_window_len: the degree to smooth the lines

    Outputs:
    - save the three lines in self, e.g.
      >>> A = DTA(...)
      >>> A.calculate()
      >>> A.lines['baseline'] # return baseline
      ...
    """
    anomaly_num = 500
    base_data_norm = normalize(self.base_data, norm='max')
    mu = self.base_data.mean(axis=0)
    mu_norm = base_data_norm.mean(axis=0)
    diff_data = base_data_norm - mu_norm
    r = pyasl.generalizedESD(diff_data.ravel(), anomaly_num, alpha=0.05, fullOutput=False)
    anomaly_index = np.array(r[1])
    base_data_no_anomaly = base_data.copy()
    base_data_no_anomaly[np.unravel_index(anomaly_index, (days, ppd))] = np.nan
    mu_no_anomaly = np.nanmean(base_data_no_anomaly, axis=0)
    upper_sigma = np.nanstd((base_data_no_anomaly - mu_no_anomaly), axis=0)
    lower_sigma = np.nanstd((base_data_no_anomaly - mu_no_anomaly), axis=0)
    base_data_flat = base_data_no_anomaly

    base_data = base_data_flat.reshape((-1, ppd))
    upper_thresh = smooth_moving(np.nanmax(base_data, axis=0) + k1 * upper_sigma, window_len=smooth_len)
    lower_thresh = smooth_moving(np.nanmin(base_data, axis=0) - k2 * lower_sigma, window_len=smooth_len)
    baseline = smooth_moving(np.nanmean(self.base_data), axis=0, window_len=smooth_len)
    self.lines = dict(baseline=baseline, upper_thresh=upper_thresh, lower_thresh=lower_thresh)
    print "Lines Calculated"