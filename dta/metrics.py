# -*- coding: utf-8 -*-
import numpy as np
from sklearn import metrics


def mean_error(y_true, y_pred):
    """ME"""
    y_true, y_pred = np.asarray(y_true), np.asarray(y_pred)
    return np.mean(y_true - y_pred)


def mean_squared_error(y_true, y_pred):
    """MSE"""
    return metrics.mean_squared_error(y_true, y_pred)


def root_mean_square_error(y_true, y_pred):
    """RMSE"""
    return np.sqrt(mean_absolute_error(y_true, y_pred))


def mean_absolute_error(y_true, y_pred):
    """MAE"""
    return metrics.mean_absolute_error(y_true, y_pred)


def mean_percentage_error(y_true, y_pred):
    """MPE"""
    y_true, y_pred = np.asarray(y_true), np.asarray(y_pred)
    return np.mean((y_true - y_pred) / y_true) * 100


def mean_absolute_percentage_error(y_true, y_pred):
    """MAPE"""
    y_true, y_pred = np.asarray(y_true), np.asarray(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# def mean_absolute_scaled_error(y_true, y_pred):
#     """MASE"""
#     y_true, y_pred=np.asarray(y_true), np.asarray(y_pred)


if __name__ == "__main__":
    y_true = [3, -0.5, 2, 7]; y_pred = [2.5, -0.3, 2, 8]
    print(mean_absolute_percentage_error(y_true, y_pred))
