# -*- coding: utf-8 -*-

import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sklearn
from sklearn import svm
from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import normalize
from statsmodels.tsa.seasonal import seasonal_decompose
from PyAstronomy.pyasl.asl.outlier import generalizedESD

from . import utils
from . import metrics
from . import threshold


"""
Introduction
...
"""
MINUTES_PER_DAY = 1440

def clean_data(df, freq=1440, date_ignore=None):
    """
    Clean data (only one column).
    """
    dff = utils.get_workday_data(utils.fix_df(df, method='more'), date_ignore=date_ignore)
    data_re = dff.values.reshape((-1, 1440))
    data_re_norm = normalize(data_re, norm='max')
    if sklearn.__version__ >= '0.22':
        clf = IsolationForest(behaviour='new', contamination='auto', n_jobs=-1)
        labels = clf.fit_predict(data_re_norm)
    elif sklearn.__version__ >= '0.20':
        clf = IsolationForest()
        labels = clf.fit_predict(data_re_norm)
    else:
        clf = IsolationForest().fit(data_re_norm)
        labels = clf.predict(data_re_norm)
    return data_re[labels == 1]


def trend_and_seasonal(data, freq=1440):
    """Decompose data to get trend and seasonal."""
    try:
        result = seasonal_decompose(data.ravel(), model='additive', freq=freq)
    except ValueError:
        raise ValueError('数据量太少')
    trend = result.trend.reshape((-1, freq))[1:-1]
    seasonal = result.seasonal[-freq:][:, None]
    return trend, seasonal


def forecast_baseline(df, freq=1440, window_len=1, length=1440):
    """
    Forecast baseline based on STL decompose.
    """
    data = clean_data(df, freq)
    length = int(length)
    trend, seasonal = trend_and_seasonal(data, freq=freq)
    n = trend.shape[0]
    x = np.linspace(0, n, n, endpoint=False)[:, None]
    clf = svm.SVR(gamma='auto').fit(x, trend.mean(axis=1))
    y = clf.predict(np.arange(n, n+length//freq+1)[:, None])
    baseline = utils.smooth((y + np.tile(seasonal, length//freq+1)).ravel(order='F'), window_len=window_len)
    baseline = np.maximum(baseline, 0)
    return baseline[:length]


def forecast_dynamic_threshold(baseline, min_thresh=None, k=(0.2, 0.3, 0), window_len=1, lower_bounds=None, upper_bounds=None):
    """Forecast dynamic thresh based on baseline."""
    kl, ku, kn = k
    return threshold.dynamic_threshold(baseline=baseline,
                                       min_thresh=min_thresh,
                                       kl=kl,
                                       ku=ku,
                                       window_len=window_len,
                                       lower_bounds=lower_bounds,
                                       upper_bounds=upper_bounds)


def forecast_static_baseline(lower_bounds, upper_bounds, length=1440):
    return threshold.static_threshold(lower_bounds=lower_bounds,
                                      upper_bounds=upper_bounds,
                                      length=length)


def detect_anomaly(baseline, dynamic_threshold, test_true=None, verbose=0, figsize=(16, 6), title=None):
    """
    Detect anomaly based on dynamic_threshold.
    """
    lower_thresh, upper_thresh = dynamic_threshold
    if test_true is not None:
        test_true = np.nan_to_num(test_true).ravel()
        min_length = min(test_true.size, lower_thresh.size)
        test_true = test_true[:min_length]
        lower_thresh = lower_thresh[:min_length]
        upper_thresh = upper_thresh[:min_length]
        anomaly_idx0 = np.where(
            np.any([test_true < lower_thresh, test_true > upper_thresh], axis=0))[0]
        anomaly_idx1 = np.array(generalizedESD(test_true - baseline[:min_length],
                                                   maxOLs=np.maximum(anomaly_idx0.size, 1))[1], dtype='int64')
        anomaly_idx = np.intersect1d(anomaly_idx0, anomaly_idx1)
        anomaly = np.zeros((min_length,))
        anomaly[anomaly_idx] += 1
    if verbose:
        n = dynamic_threshold.shape[1]
        t = np.linspace(0, n, n, endpoint=False)
        fig = plt.figure(figsize=figsize)
        ax1 = fig.add_subplot(111)
        ax2 = fig.add_subplot(111, sharex=ax1, frameon=False)
        ax2.yaxis.tick_right()
        ax1.set_ylabel('Test Data')
        ax2.set_ylabel('Anomaly Score')
        ax2.set_ylim([-0.01, 1.1])
        ax2.yaxis.set_label_position("right")
        ax1.plot(baseline, 'g--', label='Baseline')
        ax1.fill_between(t, dynamic_threshold[0], dynamic_threshold[1],
                         color=[.5, .5, .5], alpha=.5, label='Dynamic Threshold')
        if test_true is not None:
            ax1.plot(test_true, 'b', label='Test True')
            ax2.plot(anomaly, 'r', label='Anomaly', alpha=0.5)
            # ax1.plot(test_true - baseline[:min_length, np.newaxis], label='Residual')
        # plt.title('DTA4.0: {0}: {1}'.format(self._name, date))
        if title is None:
            plt.title('Dynamic Threshold Algorithm')
        else:
            plt.title('Dynamic Threshold Algorithm: {}'.format(title))
        ax1.legend(loc=2)
        if test_true is not None:
            ax2.legend(loc=1)
        plt.show()
    if test_true is not None:
        return anomaly


def accuracy(y_true, y_pred):
    assert y_true.shape == y_pred.shape, "y_true[{}] and y_pred[{}] must have the same shape!".format(y_true.shape, y_pred.shape)
    ME = metrics.mean_error(y_true, y_pred)
    RMSE = metrics.root_mean_square_error(y_true, y_pred)
    MAE = metrics.mean_absolute_error(y_true, y_pred)
    MPE = metrics.mean_percentage_error(y_true, y_pred)
    MAPE = metrics.mean_absolute_percentage_error(y_true, y_pred)
    return pd.DataFrame(data=np.array([[ME, RMSE, MAE, MPE, MAPE]]),
                        columns=['ME', 'RMSE', 'MAE', 'MPE', 'MAPE'],
                        index=['value'])


if __name__ == "__main__":
    # sys.path.insert(0, os.path.dirname(
    #     os.path.dirname(os.path.abspath(__file__))))
    # from dta import utils
    import metrics
    pass
