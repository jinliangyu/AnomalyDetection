# -*- coding: utf-8 -*-

import numpy as np
from . import utils


def dynamic_threshold(baseline, min_thresh=None, kl=0.1, ku=0.1, window_len=1, lower_bounds=None, upper_bounds=None):
    """
    Calculate dynamic threshold based on baseline.
    """
    if min_thresh is None:
        min_thresh = np.max(baseline) / 40
    # basic threshold
    lower_thresh_base = np.maximum(utils.smooth(
        baseline - min_thresh, window_len=window_len), 0)
    upper_thresh_base = np.maximum(utils.smooth(
        baseline + min_thresh, window_len=window_len), 0)
    # adjust threshold and add noise
    lower_thresh = utils.smooth(utils.adjust_low(lower_thresh_base, kl), window_len=window_len)
    upper_thresh = utils.smooth(utils.adjust_up(upper_thresh_base, ku), window_len=window_len)
    # lower_thresh must be >= lower_bounds, default is 0
    if lower_bounds:
        lower_thresh = np.maximum(lower_thresh, lower_bounds)
    else:
        lower_thresh = np.maximum(lower_thresh, 0)
    # upper_thresh must be <= upper_bounds, default is None
    if upper_bounds:
        upper_thresh = np.minimum(upper_thresh, upper_bounds)
    # return np.array(lower_thresh, dtype='float64'), np.array(upper_thresh, dtype='float64')
    return np.asarray([lower_thresh, upper_thresh], dtype='float64')


def static_threshold(lower_bounds, upper_bounds, length=1440):
    """
    Calculate static threshold.
    """
    lower_thresh = lower_bounds * np.ones((length,))
    upper_thresh = upper_bounds * np.ones((length,))
    return np.asarray([lower_thresh, upper_thresh], dtype='float64')
