"""
Test anomaly.py
Author: jly
"""

from __future__ import print_function, division

import os
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import core.datatools as dt
from core.anomaly import AnomalyDetector


def main():
    pass


if __name__ == '__main__':
    main()
