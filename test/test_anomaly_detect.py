﻿#!usr/bin/env python2
# -*- coding:utf-8 -*-
from __future__ import division, print_function

import argparse
import os
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from core.anomaly import AnomalyDetector
import core.datatools as dt
from tools.tools import file_row2col


"""
    动态阈值算法4.0 测试Demo
    作者：靳良玉
    日期：2018/01/29

    使用示例：
    python .\test_anomaly_detect.py -f '..\data\cpu_trade.csv' -s '2017-10-09' -e '2017-11-09'

    注意：
    1. 数据默认每分钟一个点，时间从00:00:00到23:59:59，一共1440个点
    2. 数据格式跟grafana导出的一致
    3. 数据尽量选取“整体幅度比较稳定”的一个月左右或者更长的时间
"""


def args_parser():
    """
    解析传入参数：
    -f, --file: 保存测试数据的csv文件；
    -s, --start: 开始测试日期
    -e, --end: 截至测试日期
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file',
                        help='The csv file contains DataFrame.')
    parser.add_argument('-s', '--start',
                        help='The start date for test.')
    parser.add_argument('-e', '--end',
                        help='The end date for test')
    args = parser.parse_args()
    return args.file, args.start, args.end


def main():
    # 解析传入参数
    file, start, end = args_parser()

    # 载入原始数据
    # 格式转化
    print('#'*20, '\tLoad Test Data\t', '#'*20)
    file_row2col(file)
    raw_data = dt.load_csv(file)

    # 定义节日以国庆为例
    guoqing = dt.date_list(start='2017-10-01',
                           end='2017-10-08',
                           freq='D')
    other = ['2018-01-01']  # 其他非周六、日假期

    # 处理开始和结束日期
    if start is None:
        start = dt.get_date_list(raw_data)[0]
    if end is None:
        end = dt.get_date_list(raw_data)[-1]

    # 初始化Anomaly Detector
    print('#'*20, '\tBuild Anomaly Detector\t', '#'*20)
    my_detector = AnomalyDetector(data_frame=raw_data,
                                  holiday_list=guoqing + other)
    # 开始检测。如果想保存图像，修改savefig=True
    print('#'*20, '\tShow Detect Results\t', '#'*20)
    anomaly_pool = my_detector.detect(raw_data.loc[start:end],
                                      verbose=1,
                                      savefig=False,
                                      upper_bounds=None)
    # 打印异常池
    # print(anomaly_pool)


if __name__ == '__main__':
    main()
