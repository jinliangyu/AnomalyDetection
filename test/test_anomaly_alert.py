#!usr/bin/env python2
# -*- coding:utf-8 -*-
"""
Test anomaly alert.py
Author: jly
"""

from __future__ import print_function, division
import argparse
import os
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import pandas as pd

import core.datatools as dt
from core.anomaly import AnomalyDetector
from tools.tools import file_row2col


def args_parser():
    """
    解析传入参数：
    -f, --file: 保存测试数据的csv文件；
    -s, --start: 开始测试日期
    -e, --end: 截至测试日期
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file',
                        help='The csv file contains DataFrame.')
    parser.add_argument('-s', '--start',
                        help='The start date for test.')
    parser.add_argument('-e', '--end',
                        help='The end date for test')
    args = parser.parse_args()
    return args.file, args.start, args.end


def main():
    # 解析传入参数
    file, start, end = args_parser()

    # 载入原始数据
    # 格式转化
    print('#'*20, '\tLoad Test Data\t', '#'*20)
    file_row2col(file)
    raw_data = dt.load_csv(file)

    # 定义节日以国庆为例
    guoqing = dt.date_list(start='2017-10-01',
                           end='2017-10-08',
                           freq='D')
    other = ['2018-08-01']  # 其他非周六、日假期

    # 处理开始和结束日期
    if start is None:
        start = dt.get_date_list(raw_data)[1]
    if end is None:
        end = dt.get_date_list(raw_data)[-1]

    # 初始化Anomaly Detector
    print('#'*20, '\tBuild Anomaly Detector\t', '#'*20)
    my_detector = AnomalyDetector(data_frame=raw_data,
                                  holiday_list=guoqing + other)

    # 展示Detect结果
    print('#'*20, '\tShow Detect Results\t', '#'*20)
    alert = my_detector.malert(raw_data.loc[start:end], k=1, verbose=1)
    # 展示告警结果
    print('#'*20, '\tShow Alert Results\t', '#'*20)
    print(alert)


if __name__ == '__main__':
    main()
