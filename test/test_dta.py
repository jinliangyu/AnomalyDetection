"""
Test DTA class
Author: jly
"""
import os
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import pandas as pd
import core.datatools as dt
from core.dta import DTA


if __name__ == '__main__':
    df = dt.load_csv('../data/rps_trade.csv')
    df = dt.preprocess(df.loc['2017-10-09':])
    dta = DTA(df, name='cpu')
    # test repr
    print '='*30
    print 'Show dta repr:', dta

    # test get k, sm, lines
    print '='*30
    print 'Show k: ', dta.k
    print 'Show smooth window len: ', dta.getsm()
    print 'Try to show lines: ', dta.lines

    # test calculate
    print '='*30
    dta.calculate()
    print 'Show calculated lines: ', dta.lines

    # test set k, sm, default_k
    print '='*30
    dta.setk((0, 0, 0))
    print 'Show changed K: ', dta.k
    dta.setsm(3)
    print 'Show changed sm: ', dta.getsm()
    dta.set_default_k((0.2, 0.3, 1))
    print 'Show changed default k: ', dta.get_default_k()

    # test detect
    print '='*30
    test_data = df.loc['2017-12-05']
    anomaly_pool = dta.detect(test_data, verbose=0)
    print 'Peek detect anomaly result: \n', anomaly_pool.head()

    # test adjust
    if 0:
        print '='*30
        print 'test  adjust: '
        dta.adjust(method='reset')
        dta.detect(test_data, verbose=1, savefig=True, name='Origin')
        print 'Default k: ', dta.k
        dta.adjust(method='wider', degree=0.9)
        dta.detect(test_data, verbose=1, savefig=True, name='Wider')
        print 'Become wider: ', dta.k

        dta.adjust(method='reset')
        dta.adjust(method='narrower', degree=0.9)
        dta.detect(test_data, verbose=1, savefig=True, name='Narrower')
        print 'Become narrower: ', dta.k

        dta.adjust(method='reset')
        dta.adjust(method='upper', degree=0.5)
        dta.detect(test_data, verbose=1, savefig=True, name='Upper')
        print 'Become lower: ', dta.k

        dta.adjust(method='reset')
        dta.adjust(method='lower', degree=0.5)
        dta.detect(test_data, verbose=1, savefig=True, name='Lower')
        print 'Become upper: ', dta.k

    # test save and restore
    if 1:
        print '='*30
        print('test save and restore: ')
        dta.save('../models')
        dta.restore('../models')

    # test update
    print '='*30
    update_df = df.loc['2017-10-15':]
    print 'Test update: '
    dta.update(update_df)

    # test end
    print '='*30
    print 'Test end! Welcome to add more test to this script!'
    print '='*30
