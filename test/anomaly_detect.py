# -*- coding: utf-8 -*-
import argparse
import os
import sys
sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')))

import numpy as np

from dta import dta, utils

def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file',
                        help='The path of csv file ')
    args = parser.parse_args()
    return args.file

if __name__ == '__main__':
    # test_file = 'test/data/spikes/181.csv'
    df = utils.load_csv(arg_parser(), time_delta='8h')
    # df = utils.load_csv(test_file, time_delta='8h')
    df = utils.fix_df(df)
    date_list = utils.get_date_list(df)
    for date in date_list[5:]:
        baseline = dta.forecast_baseline(df.loc[:date])
        min_thresh = np.max(baseline - df.loc[date].values)/10
        dynamic_threshold = dta.forecast_dynamic_threshold(baseline, min_thresh=min_thresh, k=(.0, .0, 0))
        dta.detect_anomaly(baseline, dynamic_threshold, test_true=df.loc[date], verbose=1, title=date)
