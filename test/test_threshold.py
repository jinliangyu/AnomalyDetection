# -*- coding: utf-8 -*-
import os
import sys

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


from dta import threshold

if __name__ == "__main__":
    print(threshold.static_threshold(10, 1))
    print(threshold.static_threshold(100, 1).shape)
    print(type(threshold.static_threshold(100, 1)))