# -*- coding: utf-8 -*-
"""
Test tools.py client2.py
Author: jly
"""

from __future__ import print_function, division

import os
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from tools.client2 import Connection
from tools.tools import files2opentsdb

import pandas as pd
import matplotlib.pyplot as plt


def test_read():
    connection = Connection(server='192.168.1.92', port=4242)
    """Test read_df / read_ts
    """

    # df = connection.read_df(to_json(start='2018-01-10 00:00:00',
    #                                 end='2018-01-10 23:59:59',
    #                                 metric='tomcat_raw_zabbix_jmxCatalinatypeGlobalRequestProcessor_namehttp_HTTP_PROTOCOL_HTTP_PORT_requestCount',
    #                                 tags=dict(servicecode="*")))
    # df.plot()
    # plt.show()

    # df = connection.read_df(to_json(start='2018-01-10 00:00:00',
    #                                 end='2018-01-10 23:59:59',
    #                                 metric='linux_raw_zabbix_usrparm_cpu_util',
    #                                 tags=dict(servicecode="*")))
    # df.plot()
    # plt.show()

    df = connection.read_df(to_json(start='2018-01-01 00:00:00',
                                    end='2018-01-31 23:59:59',
                                    delete=True,
                                    metric='tomcat_access_groupbyserve_avg_latency',
                                    tags=dict(servicecode="*")))
    df.plot()
    plt.show()

    # df = connection.read_df(to_json(start='2018-01-10 00:00:00',
    #                                 end='2018-01-10 23:59:59',
    #                                 metric='tomcat_biz_groupbyservice_1min_success_rate',
    #                                 tags=dict(servicecode="*")))
    # df.plot()
    # plt.show()


def test_write():
    files2opentsdb(server='192.168.1.92',
                   port=4242,
                   inpath=r'C:\Users\jinliangyu\Desktop\workplace\2018-02\backup\latency',
                   n=400)


def to_json(start, end, metric, tags, downsample="1m-avg-nan", aggregator="sum", delete=False):
    """To json.

    Arguments:
        start {str} -- the start time
        end {str} -- the end time
        metric {str} -- metric name
        tags {dict} -- tagk: tagv pairs

    Keyword Arguments:
        downsample {str} -- the downsample function (default: ("1m-avg-nan"))
        aggregator {str} -- the aggregator function (default: {"sum"})
    """
    BodyContent = \
        {
            "start": int((pd.Timestamp(start) - pd.Timedelta('8h')).value/10**9),
            "end": int((pd.Timestamp(end) - pd.Timedelta('8h')).value/10**9),
            "delete": delete,
            "queries": [
                {
                    "aggregator": aggregator,
                    "metric": metric,
                    "downsample": downsample,
                    "tags": tags
                }
            ]
        }
    return BodyContent


if __name__ == '__main__':
    # test_read()
    # to_json(start='2018-01-10 00:00:00',
    #         end='2018-01-10 23:59:59',
    #         metric='tomcat_biz_groupbyservice_1min_success_rate',
    #         tags=dict(servicecode="*"))
    test_write()
