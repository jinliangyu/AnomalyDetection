from __future__ import print_function
from time import time

import pandas as pd
import numpy as np
from dta import DTA
# import matplotlib.pyplot as plt

import datatools as dt

"""
Anomaly Detector
Author: jly
"""

__version__ = '1.1'


class AnomalyDetector(object):
    """
    Anomaly Detector for multi-metric data frame
    """

    def __init__(self, data_frame=None, holiday_list=[]):
        self._metrics = []
        self._calc = False
        self._lines = None
        self._dta_dict = {}
        # self._corr = data_frame.corr(method='pearson')
        if data_frame is not None:
            self.fit(data_frame, holiday_list=holiday_list)

    @property
    def lines(self):
        if self._calc:
            return self._lines
        for dta in self._dta_dict.values():
            dta.calculate()
        self._calc = True
        self._lines = {i: self._dta_dict[i].lines for i in self._metrics}
        return {i: self._dta_dict[i].lines for i in self._metrics}

    def fit(self, data_frame=None, holiday_list=[]):
        start = time()
        columns = data_frame.columns.tolist()
        if data_frame is not None:
            for k, v in dt.df2cols(data_frame):
                self._dta_dict[k] = DTA(df=v,
                                        name=k,
                                        holiday_list=holiday_list)
                if self._dta_dict[k].exist_model():
                    self._metrics.append(k)
                else:
                    print('{!r} Skipped'.format(k))
        end = time()
        print('DTA classes {!r} Initialized!'.format(self._metrics))
        print('Finished in {:g}s'.format(end - start))

    def detect(self, data_frame, verbose=0, savefig=False, lower_bounds=None, upper_bounds=None):
        start = time()
        frames = []
        columns = data_frame.columns.tolist()
        for i in columns:
            istart = time()
            if i in self._metrics:
                if not self._calc:
                    self._dta_dict[i].calculate(lower_bounds=lower_bounds, upper_bounds=upper_bounds)
                frames.append(self._dta_dict[i].detect(
                    data_frame[[i]], verbose=verbose, savefig=savefig))
            else:
                print('No DTA for metric: {!s}! Skipped!'.format(i))
            iend = time()
            print('Cost {:g}s'.format(iend - istart))
        end = time()
        print('Finished in {:g}s'.format(end - start))
        return pd.concat(frames, axis=1) if frames else None

    def adjust(self, kwargs):
        """
        'kwargs' contains the adjust parameters of each metric.
        Format like this:
            ('TRADE', +0.5)
            ('FINANCE', -0.5)
            ('SHOUYE', 0)
        '+': adjust up
        '-': adjust down
        '0': reset to original
        """
        if self._calc:
            for metric, k in kwargs.iteritems():
                print(metric, k)
                if k > 0:
                    self._dta_dict[metric].adjust(method='upper', degree=k)
                elif k < 0:
                    self._dta_dict[metric].adjust(method='lower', degree=-k)
                else:
                    self._dta_dict[metric].adjust(method='reset', degree=0)
            self._lines = {i: self._dta_dict[i].lines for i in self._metrics}
        else:
            raise Exception('Lines to be calculated!')

    def ialert(self, data_frame, kwargs):
        """
        isolation-metric alert
        'kwargs' contains the alert parameters of each metric.
        Format like this:
            ('TRADE', 3)
            ('FINANCE', 2)
            ('SHOUYE', 5)
        """

        anomaly_pool = self.detect(data_frame)
        alert_dict = {}
        for metric, k in kwargs.iteritems():
            if k <= 0:
                raise Exception('Invalid k, must be >=1!')
            alert = anomaly_pool[[metric]].rolling(
                k).sum().fillna(method='bfill')
            alert_dict[metric] = alert.loc[(alert >= k).any(
                axis=1) == True].index.tolist()
        return alert_dict

    def malert(self, data_frame, k=3, verbose=0):
        """
        multi-metric alert
        k is the common parameter for related metrics
        """
        anomaly_pool = self.detect(data_frame, verbose=verbose)
        alert = anomaly_pool.rolling(k).sum().fillna(method='bfill')
        return alert.loc[(alert >= k).all(axis=1) == True].index.tolist()

    def ralert(self, data_frame, k=3, verbose=0):
        pass
