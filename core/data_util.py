# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

__version__ = '0.0.1'


def read_csv(filename, time_delta='0h', sep=';'):
    if not filename.endswith('.csv'):
        return None
    with open(filename) as f:
        first_line = f.readline()
        if first_line == 'sep=;\n':
            second_line = f.readline()
            if second_line == 'Series;Time;Value\n':
                print('Do row2col!')
                data = row2col(pd.read_csv(filename, sep=';', skiprows=1))
            else:
                data = pd.read_csv(filename, index_col=0, sep=';', skiprows=1)
        else:
            data = pd.read_csv(filename, index_col=0, sep=';')
    data.index = pd.to_datetime(data.index) + pd.Timedelta(time_delta)
    return data

def to_csv(filename, df):
    if not filename.endswith('.csv'):
        filename += '.csv'
    df.to_csv(filename, sep=';')

def fix_data(df, method='default'):
    """
    Repair data by filling NaN and interpolation(linear).
    
    method(str): default, more
    """
    if method == 'default':
        df_re = df.reindex(pd.date_range(start=df.index[0],
                                         end=df.index[-1],
                                         freq='T'))
    elif method == 'more':
        date_list = sorted({i.strftime('%Y-%m-%d') for i in df.index})
        frames = []
        for date in date_list:
            new_idx = pd.date_range(start=date+' '+'00:00:00',
                                    end=date+' '+'23:59:59',
                                    freq='T')
            frames.append(df.loc[date].reindex(new_idx))
        df_re = pd.concat(frames, axis=0)
    else:
        print("Only two methods: default/more!")
        return 
    return df_re.interpolate(limit_direction='both')
    
def view_data(data, figsize=(14, 5), title=None):
    """
    Have a look at the data of numpy.array/pd.DataFrame.
    :param data: the np.ndarray or pd.Dataframe to view
    :param figsize: tuple of (width, height)
    :param title: str of title of the figure
    :return:
    """
    plt.figure(figsize=figsize)
    plt.plot(data)
    if title:
        plt.title(title)
    plt.show()

def smooth(x, window_len=11, window='hanning'):
    """Smooth the data using a window with requested size.

    Arguments:
        x {np.ndarray} -- the input signal

    Keyword Arguments:
        window_len {int} -- the dimension of the smoothing window; should be an odd integer (default: {11})

    Raises:
        ValueError -- the dimension of x not equals 1
        ValueError -- the size of x is smaller than the window_len
        ValueError -- windon_len smaller than 3

    Returns:
        np.ndarray -- the smoothed signal
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")
    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")
    if window_len < 3:
        return x
    if window not in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError(
            "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")
    s = np.r_[x[window_len - 1:0:-1], x, x[-2:-window_len - 1:-1]]
    if window == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('np.' + window + '(window_len)')
    y = np.convolve(w / w.sum(), s, mode='valid')
    return y[(window_len - 1) // 2:-(window_len - 1) // 2]

def to_date_list(datetimeindex):
    """Convert datetimeindex to list of date.

    Arguments:
        datetimeindex {pd.DateTimeIndex} -- pandas DateTimeIndex

    Returns:
        list of str -- date list
    """

    return sorted({i.strftime('%Y-%m-%d') for i in datetimeindex})


def date_list(start, end, freq='D'):
    """Use pandas.date_range to generate date_list.

    Arguments:
        start {str} -- Left bound for generating dates
        end {str} -- Right bound for generating dates

    Returns:
        list of str -- list of date
    """

    return to_date_list(pd.date_range(start=start,
                                      end=end,
                                      freq=freq))


def get_date_list(df):
    """
    return a list of date from DataFrame with format '%Y-%m-%d'
    """
    return sorted({i.strftime('%Y-%m-%d') for i in df.index})


def generate_workdate_list(date_list, holiday_list=[]):
    """
    return workday date by filtering date_list
    """
    if isinstance(date_list, list) and isinstance(date_list[0], str):
        bussiness = {i.strftime(
            '%Y-%m-%d') for i in pd.date_range(start=date_list[0], end=date_list[-1], freq='B')}
        return sorted(set(bussiness) - set(holiday_list))
    else:
        raise Exception('Argument must be a non-empty list of str!')


def get_workdate_list(df, holiday_list=[]):
    """
    return a list of workdate (workday) from DataFrame with format '%Y-%m-%d'
    """
    return generate_workdate_list(get_date_list(df), holiday_list=holiday_list)


def get_workday_data(df, holiday_list=[]):
    """Return dataframe of working date.

    Arguments:
        df {pd.DataFrame} -- original dataframe

    Returns:
        pd.DataFrame -- dataframe which only contains working date data 
    """

    workdate_list = get_workdate_list(df, holiday_list=holiday_list)
    return pd.concat([df.loc[i] for i in workdate_list], axis=0) if workdate_list else None


def check_data(df, verbose=0):
    """Check the quality the dataframe.

    Arguments:
        df {pd.DataFrame} -- dataframe to be checked

    Keyword Arguments:
        verbose {int} -- 1: show details of each day 
                         0: show summary information
                         (default: {0})
    """

    date_list = get_date_list(df)
    missing_date = []
    for i in date_list:
        data = df.loc[i]
        nans = float(pd.isnull(data).sum().sum())
        if verbose == 1:
            print('{!r}: Shape: {!r} . NaN: {:.0f}({:.2f}%)'.format(
                i, data.shape, nans, nans / data.size * 100))
        if data.shape[0] != 1440:
            missing_date.append(i)
    print('Datetime Range: [{!s}] ~ [{!s}]'.format(df.index[0], df.index[-1]))
    print('Columns: {!s}'.format(df.columns.tolist()))
    print('Points Per Day: {:g}'.format(df.shape[0] / len(date_list)))
    if verbose == 1:
        print(
            'Missing Data: {!r}/{!r}({:.2f}%)\nMissing Date: {!r}\nNan Percentage: {:.2f}%'.format(len(missing_date), len(date_list),
                                                                                     len(missing_date) / len(
                                                                                         date_list) * 100, missing_date,
                                                                                     float(pd.isnull(
                                                                                         df).sum().sum()) / df.size * 100))
        return (
            'Missing Data: {!r}/{!r}({:.2f}%)\nMissing Date: {!r}\nNan Percentage: {:.2f}%'.format(len(missing_date), len(date_list),
                                                                                     len(missing_date) / len(
                                                                                         date_list) * 100, missing_date,
                                                                                     float(pd.isnull(
                                                                                         df).sum().sum()) / df.size * 100))
    else:
        print('Missing Data: {!r}/{!r}({:.2f}%)\nNan Percentage: {:.2f}%'.format(len(missing_date), len(
            date_list), len(missing_date) / len(date_list) * 100, float(pd.isnull(df).sum().sum()) / df.size * 100))
        return('Missing Data: {!r}/{!r}({:.2f}%)\nNan Percentage: {:.2f}%'.format(len(missing_date), len(
            date_list), len(missing_date) / len(date_list) * 100, float(pd.isnull(df).sum().sum()) / df.size * 100))


def fillnan(df):
    """Fill the missing data poinrts with NaN.

    Arguments:
        df {pd.DataFrame} -- dataframe with missing data, assuming that each day has 1440 points

    Returns:
        pd.DataFrame -- dataframe without missing data points
    """

    date_list = get_date_list(df)
    frames = []
    for date in date_list:
        new_idx = pd.date_range(start=date + ' ' + '00:00:00',
                                end=date + ' ' + '23:59:59', freq='T')
        # default fill np.NaN
        frames.append(df.loc[date].reindex(new_idx))
    return pd.concat(frames, axis=0)


def filter_data(df, max_nan=0.1):
    """Filter data by max_nan.

    Arguments:
        df {pd.DataFrame} -- dataframe with NaNs

    Keyword Arguments:
        max_nan {float} -- max NaN percentage (default: {0.1})

    Returns:
        pd.DataFrame -- dataframe with NaN percentage under max_nan
    """

    date_list = get_date_list(df)
    frames = []
    for i in date_list:
        data = df.loc[i]
        nan_percentage = float(pd.isnull(data).sum().sum()) / data.size
        if nan_percentage <= max_nan:
            frames.append(data)
    return pd.concat(frames, axis=0) if frames else None


def preprocess(df, holiday_list=[], max_nan=0.1):
    """Preprocess original dirty data.

    Arguments:
        df {pd.DataFrame} -- original dirty data

    Keyword Arguments:
        holiday_list {list of str} -- no-working date except weekends (default: {[]})
        max_nan {float} -- the max percentage of NaNs (default: {0.1})

    Returns:
        pd.DataFrame -- clear data
    """

    return filter_data(fillnan(get_workday_data(df=df,
                                                holiday_list=holiday_list)),
                       max_nan=max_nan).interpolate(limit_direction='both')


def generate_df(ndarray, date, columns):
    """Generate a day dataframe by a 1440-size np.ndarray, a date and a column name.

    Arguments:
        ndarray {np.ndarray} -- a ndarray which size is 1440
        date {str} -- the date
        column {list of str} -- the metric name

    Returns:
        pd.DataFrame -- the dataframe of a day
    """

    return pd.DataFrame(data=ndarray,
                        index=pd.date_range(
                            start=' '.join([date, '00:00:00']),
                            end=' '.join([date, '23:59:59']),
                            freq='T'),
                        columns=columns)


def df2cols(df):
    """Generator for creating dataframe's each column.

    Arguments:
        df {pd.DataFrame} -- a dataframe with one or more columns

    Yields:
        str, pd.DataFrame -- column name with its dataframe
    """

    for col in df.columns:
        yield str(col), df[[col]]
