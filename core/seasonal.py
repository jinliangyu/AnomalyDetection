# -*- coding: utf-8 -*-
from random import choice

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import data_util as du

freq = 1440


def adjust_up(x, k):
    """
    Adjust upper.
    """
    return (1 + k * x / x.max()) * x


def adjust_low(x, k):
    """
    Adjust lower.
    """
    return (1 - k * x / x.max()) * x


def smooth(X, window_len=11, window='hanning'):
    return np.array([du.smooth(x, window_len, window) for x in X.T]).T


def str_decompose(df):
    date_list = du.get_date_list(df)
    df_re = du.get_workday_data(du.fix_data(
        df.loc[date_list[-10]:], method="more"))
    trend = df_re.rolling(1440, center=True, axis=0).mean()
    detrended = np.array(df_re - trend)
    # seasonal = np.array([pd.core.nanops.nanmean(
    #     detrended[i::freq], axis=0) for i in range(freq)])
    seasonal = np.array([pd.core.nanops.nanmedian(
        detrended[i::freq], axis=0) for i in range(freq)])
    resid = detrended - \
        np.tile(seasonal.T, detrended.shape[0] // seasonal.shape[0]).T
    return seasonal, trend, resid


def detect_anomaly(test_df, train_df):
    upper_thresh, lower_thresh = dynamic_threshold(
        train_df, adjust=0.5, sm_win_len=11)
    whole_day_datetime = pd.Series(pd.date_range(start=test_df.index[0],
                                                 end=test_df.index[-1],
                                                 freq='T'))
    start = whole_day_datetime[whole_day_datetime == test_df.index[0]].index[0]
    end = whole_day_datetime[whole_day_datetime == test_df.index[-1]].index[0]
    test_data = test_df.values
    anomaly_idx = np.where(np.any([test_data < lower_thresh[start:end],
                                   test_data > upper_thresh[start:end]], axis=0))[0]
    print(test_data < lower_thresh[start:end])


def dynamic_threshold(df, lower_const=0, upper_const=None, adjust=0.5, sm_win_len=11):
    seasonal, trend, resid = str_decompose(df)
    baseline = seasonal
    min_thresh = np.max(seasonal) / 20
    upper_trend = np.array([pd.core.nanops.nanmax(
        trend[i::freq].rolling(4).quantile(0.8), axis=0) for i in range(freq)])
    lower_trend = np.array([pd.core.nanops.nanmin(
        trend[i::freq].rolling(4).quantile(0.2), axis=0) for i in range(freq)])
    upper_min_thresh = np.array([pd.core.nanops.nanmax(
        resid[i::freq], axis=0) for i in range(freq)])
    lower_min_thresh = np.array([pd.core.nanops.nanmin(
        resid[i::freq], axis=0) for i in range(freq)])
    upper_min_thresh = 0
    lower_min_thresh = 0
    upper_thresh = smooth(adjust_up(
        baseline + upper_trend + upper_min_thresh + min_thresh, adjust), window_len=sm_win_len)
    lower_thresh = smooth(adjust_low(
        baseline + lower_trend + lower_min_thresh - min_thresh, adjust), window_len=sm_win_len)
    if upper_const is None:
        upper_const = np.max(upper_thresh)
    return np.maximum(np.minimum(upper_thresh, upper_const), lower_const), np.minimum(np.maximum(lower_thresh, lower_const), upper_const)


if __name__ == "__main__":
    t = du.read_csv('../data/2017/trade_perhost.csv')
    date_list = du.get_date_list(t)
    # upper_constant_thresh = 3.4
    lower_constant_thresh = 0.0
    for d in range(13, len(date_list)):
        print("Train date: {} ~ {}".format(du.get_date_list(
            t.loc[:date_list[d-1]])[0], du.get_date_list(t.loc[:date_list[d-1]])[-1]))
        print("Test date: {}".format(du.get_date_list(t.loc[date_list[d]])))
        test = t.loc[date_list[d]].values
        upper_thresh, lower_thresh = dynamic_threshold(
            t.loc[:date_list[d-1]], adjust=0.5, sm_win_len=11)
        detect_anomaly(t.loc[date_list[d]], t.loc[:date_list[d-1]])
        # for i in range(upper_thresh.shape[1]):
        #     plt.figure(figsize=(15, 5))
        #     x = np.linspace(0, freq - 1, freq)
        #     plt.fill_between(x, lower_thresh[:, i].ravel(
        #     ), upper_thresh[:, 1].ravel(), color="grey", alpha=0.3)
        #     # plt.plot(x, upper_constant_thresh*np.ones([freq, 1]), 'r--')
        #     plt.plot(x, lower_constant_thresh*np.ones([freq, 1]), 'r--')
        #     plt.plot(test[:, i])
        #     plt.show()
