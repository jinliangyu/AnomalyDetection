# -*- coding: utf-8 -*-

"""
Basic detectors and sampled parameters. MA(Moving average), EWMA(Exponentially Weighted MA), TSD(Time Series Decomposition), 
SVD(Singular Value Decomposition), win(dow), freq(uency)

In total: 14 basic detectors:
- Simple threshold
- Diff
- Simple MA
- Weighted MA
- MA of diff
- EWMA 
- TSD
- TSD MAD
- Historical average
- Historical MAD
- Holt-Winters
- SVD
- Wavelet
- ARIMA
"""
import numpy as np


def _input(x):
    x = np.asarray(x, dtype='float64')
    if x.ndim == 1:
        return x[np.newaxis, :]
    elif x.ndim == 2:
        return x
    else:
        raise ValueError



def simple_threshold(x, threshold):
    x, threshold = _input(x), _input(threshold)
    if threshold.shape[0] == 1:
        print('single threshold: {}'.format(threshold))
    elif threshold.shape[0] == 2:
        lower_thresh, upper_thresh = threshold
        print(lower_thresh < upper_thresh)
        if np.all(lower_thresh > upper_thresh):
            lower_thresh, upper_thresh = upper_thresh, lower_thresh
        elif np.any(lower_thresh > upper_thresh):
            raise ValueError('Invalid threshold!')
        else:
            pass
        print('lower_threshold: {}\nupper_threshold: {}'.format(lower_thresh, upper_thresh))
    else:
        raise ValueError('Wrong threshold! Only accept single or double threshold.')


def svd(x):
    x = _input(X)
    return 

if __name__ == '__main__':
    x = [1, 2, 3, 4, 5, 6]
    lower_thresh = [2, 2, 2, 2, 2, 2]
    upper_thresh = [5, 5, 5, 5, 5, 5]
    threshold = [3, 3, 3, 3, 3, 3]
    simple_threshold(x, threshold)
    simple_threshold(x, [upper_thresh, lower_thresh])
    
