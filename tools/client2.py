# -*- coding: utf-8 -*-
"""
Build a client for reading and writing data from/to OpenTSDB.
"""
import socket
# from time import sleep
import pandas as pd
import re

import progressbar
import requests

__version__ = '0.1.0'

def ping(host, port):
    """
    Ping http://host:port.
    :param host: str, host ip
    :param port: int, port
    :return: bool, whether connection has been built
    """

    try:
        socket.socket().connect((host, port))
        print('Ping {!s}:{!s} ... OpenTSDB connection status: OK'.format(
            host, port))
        return True
    except socket.error as err:
        if err.errno == socket.errno.ECONNREFUSED:
            raise Exception('Can\'t connect to OpenTSDB Server')
        raise Exception('Fail to test OpenTSDB connection status')


def to_json(start, end, metric, tags, downsample="1m-avg-nan", aggregator="sum", delete=False):
    """
    To json.
    :param start: {str} -- the start time
    :param end: {str} -- the end time
    :param metric: {str} -- metric name
    :param tags: {dict} -- {tagk: tagv} pair
    :param downsample: {str} -- the downsample function (default: ("1m-avg-nan"))
    :param aggregator: {str} -- the aggregator function (default: {"sum"})
    :param delete: {bool} -- whether to delete
    :return: the dict of json
    """
    if isinstance(start, str):
        start = int((pd.Timestamp(start) - pd.Timedelta('8h')).value / 10 ** 9)
    if isinstance(end, str):
        end = int((pd.Timestamp(end) - pd.Timedelta('8h')).value / 10 ** 9)
    return \
        {
            "start": start,
            "end": end,
            "delete": delete,
            "queries": [
                {
                    "aggregator": aggregator,
                    "metric": metric,
                    "downsample": downsample,
                    "tags": tags
                }
            ]
        }


class Connection(object):
    """Class of Connection to OpenTSDB.
    If a connection is built, you can read and write data.
    """

    # unit argument for pd.to_datetime
    __unit_table = {10: 's', 13: 'ms', 16: 'ns'}

    def __init__(self, server='localhost', port=4242):
        self.server = server
        self.port = port
        # test the OpenTSDB connection status
        ping(server, port)
        self.url = 'http://{!s}:{:d}'.format(server, port)

    def __repr__(self):
        return '<Connection to {0}:{1}>'.format(self.server, self.port)

    @staticmethod
    def __metric_tags(str_data):
        try:
            metric = str_data.split('{')[0]
            tag_list = re.search(r'{.*}', str_data).group(0)[1:-1]
            tags = dict(tuple(i.split('=')) for i in tag_list.split(', '))
            return metric, tags
        except Exception:
            raise Exception("Can't write data without tags!")

    @staticmethod
    def __format_tags(tags):
        return '{' + ', '.join(str(k) + '=' + str(v)
                               for (k, v) in tags.items()) + '}'

    @staticmethod
    def __dict2df(json_data, metric, tags, timedelta=+8):
        timestamps, data = zip(*sorted(json_data.items()))
        try:
            unit = Connection.__unit_table[len(str(timestamps[0]))]
        except Exception:
            print(len(str(timestamps[0])))
            raise Exception('Timestamp Error!')
        return pd.DataFrame(list(data),
                            index=pd.to_datetime(timestamps, unit=unit) +
                                  pd.Timedelta('{!r} h'.format(timedelta)),
                            columns=[metric + tags],
                            dtype='f8')

    def _read(self, json_data):
        """
        Use post method to query for data.
        :param json_data: the json-encoded content for querying data
        :return: the json-encoded content of a response, if any
        """

        r = requests.post(self.url + '/api/query', json=json_data)
        # check the response status
        r.raise_for_status()
        return r.json()

    def _write(self, json_data):
        """
        Use post method to query for data.
        :param json_data: the json-encoded content for putting data
        :return: the json-encoded content for putting data
        """

        r = requests.post(self.url + '/api/put?summary', json=json_data)
        # check the response status
        r.raise_for_status()
        return r.json()

    def read_df(self, json_data):
        """
        Read dataframe from OpenTSDB.
        :param json_data: the dict, query request for reading data.
        :return: pd.DataFrame
        """

        results = self._read(json_data)
        if results:
            df = pd.concat([self.__dict2df(json_data=i['dps'],
                                           metric=i['metric'],
                                           tags=self.__format_tags(i['tags']))
                            for i in results], axis=1)
            df.index.name = 'Time'
            return df
        else:
            return None

    def write_df(self, df, n=10, timedelta=+8):
        """
        Write dataframe to OpenTSDB.
        :param df: the dataframe of column data
        :param n: the number of json_data to write once
        :param timedelta: timedelta with UTC (default +8)
        :return: the dict of write results, like {failed: *, success: *}
        """

        json_list = []
        results = dict(failed=0, success=0)
        counter = 0
        with progressbar.ProgressBar(max_value=df.size) as bar:
            for i in df.index:
                for j in df.columns:
                    timestamp = (pd.Timestamp(i) - pd.Timedelta(
                        '{!r} h'.format(timedelta))).value // 10 ** 9
                    metric, tags = self.__metric_tags(j)
                    if pd.isnull(df.loc[i, j]) or df.loc[i, j] == 'undefined':
                        counter += 1
                        continue
                    json_data = dict(
                        metric=metric,
                        timestamp=int(timestamp),
                        value=float(df.loc[i, j]),
                        tags=tags)
                    json_list.append(json_data)
                    counter += 1
                    if len(json_list) == n:
                        result = self._write(json_list)
                        results['failed'] += result['failed']
                        results['success'] += result['success']
                        json_list = []
                        bar.update(counter)
            if json_list:
                result = self._write(json_list)
                results['failed'] += result['failed']
                results['success'] += result['success']
                bar.update(counter)
            # print('\nFinished! Results:{!r}\n'.format(results))
        # print('\n')
        return {'Results': results}

    def read_ts(self, json_data):
        """
        Read original response data from OpenTSDB。
        :param json_data: the dict of json-encoded content
        :return: OpenTSDB response
        """
        return self._read(json_data)

    def write_ts(self, df, n=10):
        """
        Reserved interface: write_ts.
        :param df: the dataframe of column data
        :param n: the number of json to post at one time (default: {10})
        :return: the dict of results
        """

        return self.write_df(df, n)


if __name__ == '__main__':
    conn = Connection(server='192.168.1.92', port=4242)
