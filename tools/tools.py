# -*- coding: utf-8 -*-
from __future__ import print_function
import argparse
import os

import pandas as pd

from client2 import Connection

__version__ = '2.0'


def row2col(row_data):
    """ Change "row data" to "column data".
        Here, "row data" refers to the data which is saved by "Series as rows",
        "col data" refers to ... "Series as columns".
        Save data using Grafana.

    Arguments:
        row_data {pd.DataFrame} -- series as rows, e.g.:
        Series  | Time | Value
        -----------------------
        metirc1 | ***  | ***
        -----------------------
        metric2 | ***  | ***
        -----------------------
           .    |  .   |  .
           :    |  :   |  :
        -----------------------
        metricn | ***  | ***

    Returns:
        col_data {pd.DataFrame} -- series as columns, e.g.:
        Time | metric1 | metric2| ... | metricn
        ---------------------------------------
        ***  |  ***    |  ***   | ... |   ***
    """

    groups = row_data.groupby('Series')
    frames = []
    for i, j in groups.groups.iteritems():
        frame = pd.DataFrame(data=row_data.loc[j.values, 'Value'].values,
                             index=pd.to_datetime(
                                 row_data.loc[j.values, 'Time']),
                             columns=[i])
        frames.append(frame)
    return pd.concat(frames, axis=1)


def file_row2col(filename=None):
    """Convert a single file from row data to column data.

    Keyword Arguments:
        filename {str} -- the path of file (default: {None})
    """

    if filename and filename.endswith('.csv'):
        with open(filename) as f:
            first_line = f.readline()
            # whether raw data or not
            if first_line == 'sep=;\n':
                params = {'sep': ';', 'skiprows': 1}
                data = row2col(pd.read_csv(filename, **params))
                data.to_csv(filename)
                print('Row Data! Tranformed to Column Data!')
            else:
                params = {'index_col': 0, 'sep': ',', 'skiprows': 0}
                data = pd.read_csv(filename, **params)
                data.to_csv(filename)
                print('Column Data! No need to transform!')
    else:
        print('No file!')


def files_row2col(inpath='.', outpath='.'):
    """Convert files in a folder from row data to column data.

    Keyword Arguments:
        inpath {str} -- the folder contains row data file (default: '.')
        outpath {str} -- the folder contains column data file (default: '.')
    """

    files = os.listdir(inpath)
    if not os.path.isdir(outpath):
        os.makedirs(outpath)
        print("Make output dirs {!s}".format(outpath))
    if files:
        for filename in files:
            if filename.endswith('.csv'):
                fullpath = os.path.join(inpath, filename)
                with open(fullpath) as f:
                    first_line = f.readline()
                    if first_line == 'sep=;\n':
                        second_line = f.readline()
                        if second_line == 'Series;Time;Value\n':
                            params = {'sep': ';', 'skiprows': 1}
                            data = row2col(pd.read_csv(fullpath, **params))
                            print('{!s}:\tRow Data! Tranformed! Saved!'.format(
                                filename))
                        else:
                            params = {'index_col': 0,
                                      'sep': ';', 'skiprows': 1}
                            data = pd.read_csv(fullpath, **params)
                            print('{!s}:\Column Data! Saved!'.format(
                                filename))
                        # consider 8 hours timedelta
                        data.index = pd.to_datetime(
                            data.index) + pd.Timedelta('8h')

                        data.to_csv(os.path.join(outpath, filename))
                    else:
                        params = {'index_col': 0, 'sep': ',', 'skiprows': 0}
                        data = pd.read_csv(fullpath, **params)
                        data.to_csv(os.path.join(outpath, filename))
                        print('{!s}:\tColumn Data! Saved!'.format(filename))
            else:
                continue
    else:
        print('No file in {!s}!'.format(inpath))


def files2opentsdb(server='localhost', port=4242, inpath='.', n=10):
    """Write csv files to the OpenTSDB: http://server:port.

    Keyword Arguments:
        server {str} -- the ip address of OpenTSDB
        port {int} -- the port (default: {4242})
        inpath {str} -- the folder contains csv files
        n {int} -- number of json to post at one time (default: {10})
    """

    connection = Connection(server, port)
    files = os.listdir(inpath)
    if files:
        for filename in files:
            if filename.endswith('.csv'):
                fullpath = os.path.join(inpath, filename)
                with open(fullpath) as f:
                    first_line = f.readline()
                    if 'sep=;\n' in first_line:
                        second_line = f.readline()
                        if second_line == 'Series;Time;Value\n':
                            params = {'sep': ';', 'skiprows': 1}
                            data = row2col(pd.read_csv(fullpath, **params))
                            print(
                                "{!s}: Row Data! Tranformed! Write into OpenTSDB!".format(filename))
                        else:
                            params = {'index_col': 0,
                                      'sep': ';', 'skiprows': 1}
                            data = pd.read_csv(fullpath, **params)
                            print(
                                "{!s}: Column Data! Tranformed! Write into OpenTSDB!".format(filename))
                        if '{' in str(data.columns[0]):
                            connection.write_ts(data, n)
                        else:
                            print(
                                "{!s}: Skip data without tags!".format(filename))
                    else:
                        params = {'index_col': 0, 'sep': ',', 'skiprows': 0}
                        data = pd.read_csv(fullpath, **params)
                        if '{' in str(data.columns[0]):
                            print(
                                "{!s}: Column Data! Write directly into OpenTSDB!".format(filename))
                            connection.write_ts(data, n)
                        else:
                            print(
                                "{!s}: Skip data without tags!".format(filename))
            else:
                continue
    else:
        print("No file in {!s}!".format(inpath))


def concat_csv(inpath):
    """Concatenate the .csv files to concat.csv

    Arguments:
        inpath {str} - - the folder only contains the .csv files to be conactenate
    """
    files = os.listdir(inpath)
    frames = []
    for file in files:
        fullpath = os.path.join(inpath, file)
        params = {'index_col': 0, 'sep': ',', 'skiprows': 0}
        frames.append(pd.read_csv(fullpath, **params))
    concat_df = pd.concat(frames, axis=1)
    concat_df.index.name = 'Time'
    print('Concat complete! Saved as concat.csv')
    concat_df.to_csv(os.path.join(inpath, 'concat.csv'))


if __name__ == '__main__':
    # perform files_row2col default
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inpath',
                        help='The folder contains column data.')
    parser.add_argument('-o', '--outpath',
                        help='The folder contains row data.')
    args = parser.parse_args()
    files_row2col(inpath=args.inpath, outpath=args.outpath)

    # write df to opentsdb
    # path = r'C:\Users\jinliangyu\Desktop\workplace\2018-02\newdata'
    # files2opentsdb(server='192.168.1.92', port=4242, inpath=path, n=50)
